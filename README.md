# BigRig Monorepo

A yarn workspace containing the BigRig NextJS site and express server.

Full Package list:

- @bigrig/www
- @bigrig/server
- @bigrig/interfaces
- @bigrig/eslint-config-bigrig

## Quick Start

First, install dependencies to link packages together and get your editor working.

```sh
yarn
```

Then, copy and fill out the env file.

```sh
cp .env.example .env
```

```sh
docker-compose up --build
```

## Packages

### www

The client is a NextJS site that can be run in either nextjs or Storybook modes.

#### NextJS

Run NextJS mode ([README](./packages/www/README.md))

```sh
yarn www:start
```

#### Storybook

Run in Storybook mode from workspace root ([README](./packages/www/README.md))

```sh
yarn www:storybook
```

Or run this from within `./packages/www/`:

```sh
yarn storybook
```

### Server

Run Server ([README](./packages/server/README.md))

```sh
docker-compose up --build
```

To interact with the server you have two options:

1. Import the newly generated `./packages/server/build/swagger.json` into Postman
2. Use SwaggerUI, hosted at [http://localhost:3000/docs](http://localhost:3000/docs)

If you don't already have a native MySQL GUI, you can visit <http://localhost:8080> to view it with [Adminer](https://www.adminer.org/)

## Workspace Wide Commands

Commands at the root/workspace level typically use lerna to execute a script of the same name in each package.

```sh
yarn clean # cleans cache/build/dist folders, then all node_modules
yarn format # prettier formats all js,ts,jsx,tsx,json,yml,yaml
yarn lint # eslint against all js,ts,jsx,tsx
yarn stylelint # Validates styles for css,jsx,tsx
yarn types # runs tsc in each package
yarn test # runs jest in each package
yarn markdownlint # lints .md files using workspace [config](./.markdownlint.json)
```

## Notes

- Tsoa doesn't like `Omit<>`. Use `Pick<>` instead, even if more verbose
