#!/bin/bash

cd ..

sudo rm -rf mariadb **/node_modules packages/server/build packages/server/dist packages/server/uploads/*.pdf packages/server/src/routes.ts packages/api/dist
docker system prune --volumes
docker image prune -a
