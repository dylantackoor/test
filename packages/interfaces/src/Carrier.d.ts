import { Employee, JSONResponse } from '../'
import { Equipment } from './Equipment'

// DB
export interface Carrier {
  id: number
  name: string
  email: string
  avatarURL: string

  employees?: Employee[]
  equipment?: Equipment[]

  // reefers: number
  // vans: number
  // flatbeds: number
  // powerUnits: number
}

// HTTP

export type CarrierGetAllResponse = JSONResponse<Carrier[]>
export type CarrierGetReponse = JSONResponse<Carrier>

/** TODO: remove duplication once tsoa Omit<> is figured out */
export type CarrierCreationRequest = {
  name: string
  email: string
  avatarURL: string

  // // TODO: Move to new EquipmentEntity table
  // reefers: number
  // vans: number
  // flatbeds: number
  // powerUnits: number
  // // TODO: add unique VIN

  /** TODO: remove duplication once tsoa Omit<> is figured out */
  employees?: Array<{
    payment: {
      accountNumber: number
      accountType: 'checking' | 'savings'
      routingNumber: number
      holderName: string
    }
    payRate: number

    firstName: string
    lastName: string
    email: string
    phone: string
    fax?: string

    // Address
    street: string
    city: string
    state: States
    zip: number

    // Registration Info
    DOT: number
    MC: number
    EIN: number
    /** http://www.nmfta.org/(X(1)S(pdwmiv3let4l3ms303vvbd5k))/pages/scac?AspxAutoDetectCookieSupport=1 */
    SCAC?: number
  }>
}
export type CarrierCreationResponse = JSONResponse<Carrier & { employees: Employee[] }>
