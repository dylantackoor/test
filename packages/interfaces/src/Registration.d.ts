import { Carrier, Broker, Employee, Packet, JSONResponse } from '../'

// DB

export interface Registration {
  id: number
  packet?: Packet

  carrier: Carrier
  employee: Employee
  broker: Broker
  recieved: Date
  confirmed?: Date
}

// HTTP

/** todo: fix Registration[] causing errors */
export type RegistrationGetAllRequest = Partial<Registration>
export type RegistrationGetAllResponse = JSONResponse<Registration[]>
export type RegistrationGetReponse = JSONResponse<Registration>

export interface RegistrationCreationRequest {
  fromBrokerEmail: string
  toEmployeeEmail: string
  // TODO: packetURL: string
}
export type RegistrationCreationResponse = JSONResponse<Registration>

export interface RegistrationConfirmRequest {
  packetID: number
  registrationID: number
  answers: any
}
export type RegistrationConfirmResponse = JSONResponse<true>
