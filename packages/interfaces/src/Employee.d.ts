import { BankAccount, JSONResponse, States } from '../'
import { Equipment } from './Equipment'
import { Carrier } from './Carrier'

// DB

/** Named Driver on frontend. Employee on backend to avoid interface name collisions */
export interface Employee {
  // TODO: convert to UUIDv4
  id: number

  carrier: Carrier
  payment: BankAccount
  equipment?: Equipment

  /** TODO: confirm interval */
  payRate: number

  firstName: string
  lastName: string
  email: string
  phone: string
  fax?: string

  street: string
  city: string
  state: States
  zip: number

  DOT: number
  MC: number
  EIN: number
  /** http://www.nmfta.org/(X(1)S(pdwmiv3let4l3ms303vvbd5k))/pages/scac?AspxAutoDetectCookieSupport=1 */
  // TODO: is it this? https://en.wikipedia.org/wiki/Standard_Carrier_Alpha_Code
  SCAC?: number
}

// HTTP

export type EmployeeFindRequest = Partial<Employee>
export type EmployeeFindResponse = JSONResponse<Employee>

export type EmployeeFindAllRequest = EmployeeFindRequest
export type EmployeeFindAllResponse = JSONResponse<Array<Employee>>

/** TODO: remove duplication once tsoa Omit<> is figured out */
export interface EmployeeCreationRequest {
  carrierID: number
  payment: {
    accountNumber: number
    accountType: 'checking' | 'savings'
    routingNumber: number
    holderName: string
  }
  payRate: number

  // TODO: add to entity
  // active: boolean

  firstName: string
  lastName: string
  email: string
  phone: string
  fax?: string

  // Address
  street: string
  city: string
  state: States
  zip: number

  // Registration Info
  DOT: number
  MC: number
  EIN: number
  /** http://www.nmfta.org/(X(1)S(pdwmiv3let4l3ms303vvbd5k))/pages/scac?AspxAutoDetectCookieSupport=1 */
  SCAC?: number
}
export type EmployeeCreationResponse = JSONResponse<Employee>
