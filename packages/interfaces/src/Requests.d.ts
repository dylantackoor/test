import { Request } from 'express'
import { User } from './Users'

export type JSONResponseError = { message: string; inputName?: string }

export type JSONResponse<T> = {
  data?: T
  errors?: JSONResponseError[]
}

export interface AuthedReq<T> extends Request {
  user: User
  body: T
}
