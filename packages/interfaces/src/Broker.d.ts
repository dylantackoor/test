// import { JSONSchema7 } from 'json-schema'
import { Packet, JSONResponse, PacketField } from '../'

// DB

export interface Broker {
  id: number
  name: string
  email: string
  phone: string
  avatarURL?: string
  packets: Packet[]
}

// HTTP

export type BrokerFindRequest = Partial<Broker>
export type BrokerFindResponse = JSONResponse<Broker>

export type BrokerFindAllRequest = BrokerFindRequest
export type BrokerFindAllResponse = JSONResponse<Array<Broker>>

export type BrokerCreationRequest = {
  name: string
  email: string
  phone: string
  avatarURL?: string
  packets: Array<{
    version: number
    bytes: string
    inputs: Array<{
      column: string
      preview: string
      position: {
        page: number
        x1: number
        x2: number
        y1: number
        y2: number
      }
    }>
  }>
}
export type BrokerCreationResponse = JSONResponse<{ brokerID: number }>
