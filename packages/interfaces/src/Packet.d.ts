import { JSONSchema7 } from 'json-schema'
import { Broker, JSONResponse } from '../'
// import { UiSchema } from '@rjsf/core'

export type PacketFieldTypes = 'text' | 'date' | 'image'

export interface PacketField {
  column: string
  page: number
  x1: number
  x2: number
  y1: number
  y2: number
}

export interface Packet {
  id: number
  broker: Broker
  pdf: string
  coordinates: {
    fields: PacketField[]
  }
}

export type PacketCreationRequest = {
  brokerID: number
  pdf: string
  coordinates: {
    fields: PacketField[]
  }
}
export type PacketCreationResponse = JSONResponse<true>
