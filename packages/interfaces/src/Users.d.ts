import { JSONResponse } from './Requests'

// DB

export interface User {
  /**
   * UUIDv4
   * @example "b5d9cf7c-296c-4ef3-8024-190b1e4e69d8"
   */
  id: string

  /** @example "user@bigrig.dev" */
  email: string

  /** @example "supersecret" but bcrypted */
  password: string
}

// HTTP

export type UserGetResponse = JSONResponse<{ id: string; email: string }>

export type UserCreateRequest = {
  email: string
  password: string
  passwordConfirmation: string
}
export type UserCreateResponse = JSONResponse<{
  id: string
  email: string
  token: string
}>
