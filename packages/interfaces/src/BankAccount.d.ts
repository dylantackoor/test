export interface BankAccount {
  id: number

  accountNumber: number
  accountType: 'checking' | 'savings'
  routingNumber: number
  holderName: string
}
