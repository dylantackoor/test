import { Broker } from './Broker'
import { Carrier } from './Carrier'
import { Employee } from './Employee'
import { Registration } from './Registration'
import { AuthedReq, JSONResponse } from './Requests'
import { User } from './Users'

export type BasicAuthToken = {
  /** email:password base64 encoded string */
  token: string
}

export type AuthState = {
  loading: 'idle' | 'pending' | 'succeeded' | 'failed'
  errors: JSONResponseError[]

  token: string
  user: Pick<User, 'id' | 'email'>
  drivers: Employee[]
  carriers: Carrier[]
  setups: Registration[]
  brokers: Broker[]
}

export type AuthStateResponse = Pick<
  AuthState,
  'token' | 'user' | 'drivers' | 'carriers' | 'setups' | 'brokers'
>

export type AuthLoginRequest = Pick<User, 'email' | 'password'>
export type AuthLoginResponse = JSONResponse<AuthStateResponse>

export type AuthCreateRequest = AuthLoginRequest & {
  confirmation: string
}
export type AuthCreateResponse = AuthLoginResponse

export type AuthPatchRequest = AuthedReq<
  Pick<User, 'password'> & {
    confirmation: string
  }
>
export type AuthPatchResponse = JSONResponse<BasicAuthToken>

export type AuthResetInitRequest = { email: string }
export type AuthResetConfirmRequest = Pick<User, 'id' | 'password'> & {
  token: string
  confirmation: string
}
export type AuthResetResponse = JSONResponse<true>
