import { Carrier } from './Carrier'
import { Employee } from './Employee'
import { JSONResponse } from './Requests'

export interface Equipment {
  id: number
  vin: number
  type: 'reefer' | 'van' | 'powerUnit' | 'flatbed'
  employees?: Employee[]
  carrier: Carrier
}

// HTTP

export type EquipmentFindRequest = Partial<Equipment>
export type EquipmentFindResponse = JSONResponse<Equipment>

export type EquipmentFindAllRequest = Partial<Equipment>
export type EquipmentFindAllResponse = JSONResponse<Array<Equipment>>

export type EquipmentAssignRequest = {
  equipmentID: number
  employeeIDs: number[]
}
export type EquipmentAssignResponse = JSONResponse<{
  employeeID: number
  equipment: Equipment
}>

export type EquipmentUnassignRequest = {
  employeeID: number
}
export type EquipmentUnassignResponse = JSONResponse<Pick<Employee, 'id'>>

export type EquipmentRemoveRequest = EquipmentAssignRequest
export type EquipmentRemoveResponse = EquipmentAssignResponse

export type EquipmentCreationRequest = {
  vin: number
  type: 'reefer' | 'van' | 'powerUnit' | 'flatbed'
  employeeIDs?: number[]
  carrierID: number
}
export type EquipmentCreationResponse = JSONResponse<Equipment>
