import { Request } from 'express'
import { User } from '@bigrig/interfaces'

// TODO: move to @bigrig/interfaces
export type AuthenticatedRequest = Request & { user: User }
