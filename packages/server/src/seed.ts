/* eslint-disable @typescript-eslint/no-non-null-assertion */

import { Chance } from 'chance'
import { CarrierCreationRequest } from '@bigrig/interfaces'

import { AuthService, BrokerService, CarrierService, EquipmentService } from './services'
import { UserEntity } from './entities'

const chance = new Chance()

const AuthClient = new AuthService()
const BrokerClient = new BrokerService()
const CarrierClient = new CarrierService()
const EquipmentClient = new EquipmentService()

const adminEmail = 'admin@bigrig.dev'

export const seedDB: () => Promise<void> = async () => {
  console.log('Checking if seed is needed')
  const existingAdminUser = await UserEntity.findOne({ where: { email: adminEmail } })
  if (existingAdminUser) {
    return console.log('DB already seeded, aborting')
  }

  console.log('Seeding Users')
  await AuthClient.register({
    email: adminEmail,
    password: 'test',
    confirmation: 'test',
  })

  await Promise.all(
    // @ts-expect-error idc
    new Array(chance.integer({ min: 12, max: 15 })).fill().map(async () => {
      console.log('Seeding Carriers')
      const carrierSeed: CarrierCreationRequest = {
        avatarURL: chance.avatar({ protocol: 'https' }),
        email: chance.email({ domain: 'bigrig.dev' }),
        name: chance.company(),

        // @ts-expect-error idc
        employees: new Array(chance.integer({ min: 1, max: 10 })).fill().map(() => ({
          firstName: chance.first(),
          lastName: chance.last(),
          EIN: chance.integer({ min: 0, max: 9999999 }),
          MC: chance.integer({ min: 0, max: 9999999 }),
          DOT: chance.integer({ min: 0, max: 9999999 }),
          SCAC: chance.integer({ min: 0, max: 9999999 }),
          email: chance.email(),
          street: chance.address(),
          city: chance.city(),
          state: chance.state({ us_states_and_dc: true, full: false }),
          zip: parseInt(chance.zip()),
          payRate: 0,
          phone: chance.phone({ country: 'us' }),
          fax: chance.phone({ country: 'us' }),
          payment: {
            accountNumber: chance.integer({ min: 0, max: 9999999 }),
            accountType: chance.pickone(['checking', 'savings']),
            holderName: 'Test User',
            routingNumber: chance.integer({ min: 0, max: 9999999 }),
          },
        })),
      }

      const { data: carrier, errors } = await CarrierClient.create(carrierSeed)
      if (errors) {
        console.error(JSON.stringify(errors))
        throw errors
      }

      console.log('Seeding Brokers')
      await BrokerClient.create({
        email: chance.email(),
        phone: chance.phone(),
        name: chance.company(),
        avatarURL: chance.avatar({ protocol: 'https' }),
        packets: [],
      })

      console.log('Seeing Equipment')
      // @ts-expect-error idc
      const newEquipment = new Array(chance.integer({ min: 1, max: 10 })).fill().map(() => ({
        vin: chance.integer({ min: 0, max: 99999 }),
        carrierID: carrier!.id,
        employeeIDs: chance.pickset(carrier!.employees.map(employee => employee.id)),
        type: chance.pickone(['flatbed', 'reefer', 'van', 'powerUnit']) as
          | 'flatbed'
          | 'reefer'
          | 'van'
          | 'powerUnit',
      }))

      await Promise.all(
        newEquipment.map(async equipment => {
          const result = await EquipmentClient.create(equipment)
          await EquipmentClient.assign({
            equipmentID: result.data!.id,
            employeeIDs: chance.pickset(carrier!.employees.map(employee => employee.id)),
          })
        }),
      )
    }),
  )
}
