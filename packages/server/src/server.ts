import { createConnection } from 'typeorm'
import { app } from './app'
import { seedDB } from './seed'

const { MYSQL_HOST, MYSQL_DB, MYSQL_USER, MYSQL_PASSWORD, NODE_ENV } = process.env
const entityFileType = NODE_ENV === 'production' ? 'js' : 'ts'

createConnection({
  type: 'mysql',
  host: MYSQL_HOST,
  port: 3306,
  username: MYSQL_USER,
  password: MYSQL_PASSWORD,
  database: MYSQL_DB,
  entities: [__dirname + `/entities/*.${entityFileType}`],
  synchronize: true,
})
  .then(async () => await seedDB())
  .then(() => {
    const port = process.env.PORT || '3000'
    app.listen(port, () => console.log(`listening at https://localhost:${port}`))
  })
  .catch(error => {
    console.error(error)
    throw error
  })
