import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, BaseEntity } from 'typeorm'
import { IsEmail, Contains } from 'class-validator'

import { User } from '@bigrig/interfaces'

@Entity({ name: 'users' })
export class UserEntity extends BaseEntity implements User {
  /** @example "b5d9cf7c-296c-4ef3-8024-190b1e4e69d8" */
  @PrimaryGeneratedColumn('uuid')
  id!: string

  @Column('varchar', { length: 64 })
  password!: string

  @Column('varchar', { length: 64, unique: true })
  @IsEmail()
  @Contains('@bigrig.dev')
  email!: string

  @CreateDateColumn()
  created!: Date
}
