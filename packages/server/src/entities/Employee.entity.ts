import {
  BaseEntity,
  Column,
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from 'typeorm'
import { Contains, IsAlpha, IsEmail, IsPhoneNumber, Min } from 'class-validator'
import { BankAccount, Carrier, Employee, Equipment, States } from '@bigrig/interfaces'

import { CarrierEntity } from './Carrier.entity'
import { BankAccountEntity } from './BankAccount.entity'
import { EquipmentEntity } from './Equipment.entity'

@Entity({ name: 'employees' })
export class EmployeeEntity extends BaseEntity implements Employee {
  @PrimaryGeneratedColumn()
  id!: number

  @Column('varchar')
  @IsAlpha()
  firstName!: string

  @Column('varchar')
  @IsAlpha()
  lastName!: string

  @ManyToOne(() => CarrierEntity)
  @JoinColumn()
  carrier!: Carrier

  @JoinColumn()
  @ManyToOne(() => EquipmentEntity)
  equipment?: Equipment

  @OneToOne(() => BankAccountEntity)
  @JoinColumn()
  payment!: BankAccount

  @Column('int')
  @Min(0)
  payRate!: number

  @Column('varchar', { unique: true })
  @IsEmail()
  @Contains('@bigrig.dev')
  email!: string

  @Column('varchar', { unique: true })
  @IsPhoneNumber()
  phone!: string

  @Column('varchar', { unique: true, nullable: true })
  @IsPhoneNumber()
  fax?: string

  @Column('varchar')
  street!: string
  @Column('varchar')
  city!: string
  @Column('varchar')
  state!: States
  @Column('int')
  zip!: number

  @Column('int', { unique: true })
  DOT!: number
  @Column('int', { unique: true })
  MC!: number
  @Column('int', { unique: true })
  EIN!: number
  @Column('int', { unique: true, nullable: true })
  SCAC?: number
}
