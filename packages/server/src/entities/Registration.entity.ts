import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { Broker, Carrier, Employee, Packet, Registration } from '@bigrig/interfaces'

import { PacketEntity, CarrierEntity, BrokerEntity, EmployeeEntity } from '.'

@Entity({ name: 'registrations' })
export class RegistrationEntity extends BaseEntity implements Registration {
  @PrimaryGeneratedColumn()
  id!: number

  @JoinColumn()
  @ManyToOne(() => EmployeeEntity)
  employee!: Employee

  @JoinColumn()
  @ManyToOne(() => CarrierEntity)
  carrier!: Carrier

  @JoinColumn()
  @ManyToOne(() => BrokerEntity)
  broker!: Broker

  @JoinColumn()
  @OneToOne(() => PacketEntity)
  packet?: Packet

  @CreateDateColumn({ type: 'timestamp' })
  recieved!: Date

  @Column('timestamp', { nullable: true })
  confirmed?: Date
}
