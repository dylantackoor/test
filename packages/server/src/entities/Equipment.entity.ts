import { Carrier, Employee, Equipment } from '@bigrig/interfaces'
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'

import { CarrierEntity } from './Carrier.entity'
import { EmployeeEntity } from './Employee.entity'

@Entity({ name: 'equipment' })
export class EquipmentEntity extends BaseEntity implements Equipment {
  @PrimaryGeneratedColumn()
  id!: number

  @Column()
  vin!: number

  @Column('varchar')
  type!: 'reefer' | 'van' | 'powerUnit' | 'flatbed'

  @JoinColumn()
  @ManyToOne(() => CarrierEntity)
  carrier!: Carrier

  @JoinColumn()
  @ManyToMany(() => EmployeeEntity, employee => employee.id)
  employees?: Employee[]
}
