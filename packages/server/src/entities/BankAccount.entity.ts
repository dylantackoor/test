import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'
import { BankAccount } from '@bigrig/interfaces'

@Entity({ name: 'bank_accounts' })
export class BankAccountEntity extends BaseEntity implements BankAccount {
  @PrimaryGeneratedColumn()
  id!: number

  @Column('varchar')
  holderName!: string

  @Column('varchar')
  accountType!: 'checking' | 'savings'

  @Column('int', { unique: true })
  accountNumber!: number
  @Column('int', { unique: true })
  routingNumber!: number
}
