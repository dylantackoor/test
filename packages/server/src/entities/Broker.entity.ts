import { BaseEntity, Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { IsEmail, IsPhoneNumber, IsUrl } from 'class-validator'
import { Broker, Packet } from '@bigrig/interfaces'

import { PacketEntity } from '.'

@Entity({ name: 'brokers' })
export class BrokerEntity extends BaseEntity implements Broker {
  @PrimaryGeneratedColumn()
  id!: number

  @Column('varchar')
  name!: string

  @Column('varchar')
  @IsEmail()
  email!: string

  @Column('varchar')
  @IsPhoneNumber()
  phone!: string

  @Column('varchar')
  @IsUrl()
  avatarURL?: string

  @OneToMany(() => PacketEntity, packet => packet.broker)
  @JoinColumn()
  packets!: Packet[]
}
