import { BaseEntity, Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { Carrier, Employee, Equipment } from '@bigrig/interfaces'
import { EmployeeEntity } from './Employee.entity'
import { EquipmentEntity } from './Equipment.entity'

@Entity({ name: 'carriers' })
export class CarrierEntity extends BaseEntity implements Carrier {
  @PrimaryGeneratedColumn()
  id!: number

  @Column('varchar', { unique: true })
  name!: string
  @Column('varchar', { unique: true })
  email!: string
  @Column('varchar')
  avatarURL!: string

  @OneToMany(() => EmployeeEntity, employee => employee.carrier)
  employees?: Employee[]

  @OneToMany(() => EquipmentEntity, equipment => equipment.carrier)
  equipment?: Equipment[]
}
