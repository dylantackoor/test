import { Entity, PrimaryGeneratedColumn, Column, BaseEntity, ManyToOne, JoinColumn } from 'typeorm'
import { Broker, Packet, PacketField } from '@bigrig/interfaces'
import { BrokerEntity } from './Broker.entity'
import { IsUrl } from 'class-validator'
// import { UiSchema } from '@rjsf/core'

@Entity({ name: 'packets' })
export class PacketEntity extends BaseEntity implements Packet {
  @PrimaryGeneratedColumn()
  id!: number

  // TODO: add @Contains to match against bucket URL?
  @Column('varchar')
  @IsUrl()
  pdf!: string

  @ManyToOne(() => BrokerEntity)
  @JoinColumn()
  broker!: Broker

  @Column('json')
  coordinates!: {
    fields: PacketField[]
  }
}
