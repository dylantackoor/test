import {
  EmployeeCreationRequest,
  EmployeeCreationResponse,
  EmployeeFindRequest,
  EmployeeFindResponse,
  EmployeeFindAllRequest,
  EmployeeFindAllResponse,
} from '@bigrig/interfaces'
import { QueryError } from 'mysql2'

import { CarrierEntity, EmployeeEntity } from '../entities'

export class EmployeeService {
  public async find(opts: EmployeeFindRequest): Promise<EmployeeFindResponse> {
    const employee = await EmployeeEntity.findOne(opts, {
      relations: ['packet', 'carrier', 'equipment'],
    }).catch((err: QueryError) => err)

    if (employee instanceof Error) {
      return {
        errors: [
          { message: 'Error finding matchingEmployee' },
          { message: employee.name + '\n' + employee.message },
        ],
      }
    }

    return {
      data: employee,
    }
  }

  public async findAll(opts: EmployeeFindAllRequest): Promise<EmployeeFindAllResponse> {
    try {
      const allEmployees = await EmployeeEntity.find({
        where: opts,
        relations: ['equipment', 'carrier'],
      })
      return {
        data: allEmployees,
      }
    } catch (error) {
      return { errors: [{ message: 'Error looking up employees' }] }
    }
  }

  public async create(opts: EmployeeCreationRequest): Promise<EmployeeCreationResponse> {
    const carrier = new CarrierEntity()
    carrier.id = opts.carrierID

    const employee = new EmployeeEntity()
    employee.DOT = opts.DOT
    employee.EIN = opts.EIN
    employee.MC = opts.MC
    employee.SCAC = opts.SCAC
    employee.city = opts.city
    employee.email = opts.email
    employee.fax = opts.fax
    employee.firstName = opts.firstName
    employee.lastName = opts.lastName
    employee.payRate = opts.payRate
    employee.phone = opts.phone
    employee.state = opts.state
    employee.street = opts.street
    employee.zip = opts.zip
    employee.carrier = carrier

    // TODO: employee.payment = newBankAccount

    const result = await employee.save().catch((err: QueryError) => err)
    if (result instanceof Error) {
      return { errors: [{ message: 'Could not save employee' }, { message: result.message }] }
    }

    return { data: result }
  }
}
