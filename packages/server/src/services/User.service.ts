import { UserGetResponse } from '@bigrig/interfaces'
import { UserEntity } from '../entities'

export class UserService {
  public async find(userID: string): Promise<UserGetResponse> {
    const existingUser = await UserEntity.findOne({ id: userID }, { select: ['email', 'id'] })
    if (existingUser) {
      return {
        data: existingUser,
      }
    } else {
      return {
        errors: [
          {
            message: 'User not found',
          },
        ],
      }
    }
  }
}
