import {
  EquipmentCreationRequest,
  EquipmentCreationResponse,
  // TODO: EquipmentFindAllRequest,
  EquipmentFindAllResponse,
  EquipmentFindResponse,
  EquipmentAssignRequest,
  EquipmentAssignResponse,
  EquipmentUnassignRequest,
  EquipmentUnassignResponse,
  Equipment,
} from '@bigrig/interfaces'
import { QueryError } from 'mysql2'

import { CarrierEntity, EmployeeEntity, EquipmentEntity } from '../entities'

export class EquipmentService {
  public async find(opts: Partial<Equipment>): Promise<EquipmentFindResponse> {
    const equipment = await EquipmentEntity.findOne(opts, {
      relations: ['carrier', 'employees'],
    }).catch((err: QueryError) => err)

    if (equipment instanceof Error) {
      return {
        errors: [
          { message: 'Error finding equipment' },
          { message: equipment.name + '\n' + equipment.message },
        ],
      }
    } else if (!equipment) {
      return { errors: [{ message: 'No matching equipment found' }] }
    }

    return {
      data: equipment,
    }
  }

  public async findAll(opts: Partial<Equipment>): Promise<EquipmentFindAllResponse> {
    const equipment = await EquipmentEntity.find({
      where: opts,
      relations: ['carrier', 'employees'],
    }).catch((err: QueryError) => err)

    if (equipment instanceof Error) {
      return { errors: [{ message: equipment.name + '\n' + equipment.errno }] }
    }

    return {
      data: equipment,
    }
  }

  public async create(opts: EquipmentCreationRequest): Promise<EquipmentCreationResponse> {
    const carrier = await CarrierEntity.findOne({ id: opts.carrierID }).catch(
      (err: QueryError) => err,
    )
    if (carrier instanceof Error) {
      return {
        errors: [
          { message: 'Error finding carrier' },
          { message: carrier.name + '\n' + carrier.message },
        ],
      }
    } else if (!carrier) {
      return {
        errors: [{ message: 'Unable to find matching carrier' }],
      }
    }

    // TODO: find better way to do this
    if (opts.employeeIDs) {
      const employees = await EmployeeEntity.findByIds(opts.employeeIDs).catch(
        (err: QueryError) => err,
      )

      if (employees instanceof Error) {
        return {
          errors: [
            { message: 'Error finding employees' },
            { message: employees.name + '\n' + employees.errno },
          ],
        }
      }
    }

    // TODO: create equipment
    const newEquipment = EquipmentEntity.create()
    newEquipment.type = opts.type
    newEquipment.vin = opts.vin
    newEquipment.carrier = carrier
    // if (employees.length > 0) {
    //   newEquipment.employees = employees
    // }

    const equipment = await newEquipment.save().catch((err: QueryError) => err)
    // TODO: check for errors
    if (equipment instanceof Error) {
      return {
        errors: [
          { message: 'Error saving equipment' },
          { message: equipment.name + '\n' + equipment.message },
        ],
      }
    }

    return { data: equipment }
  }

  public async assign(opts: EquipmentAssignRequest): Promise<EquipmentAssignResponse> {
    // TOOD: use this.find
    const equipment = await EquipmentEntity.findOne({ id: opts.equipmentID }).catch(
      (err: QueryError) => err,
    )

    if (equipment instanceof Error) {
      return {
        errors: [
          { message: 'Error finding equipment' },
          { message: equipment.name + '\n' + equipment.message },
        ],
      }
    }

    if (!equipment) {
      return { errors: [{ message: 'No matching equipment found' }] }
    }

    // Find employees
    const employees = await EmployeeEntity.findByIds(opts.employeeIDs).catch(
      (err: QueryError) => err,
    )
    if (employees instanceof Error) {
      return { errors: [{ message: 'Error finding employees to assign' }] }
    }

    const assignedEmployees = employees.map(employee => {
      employee.equipment = equipment
      // await employee.save()

      return employee
    })

    const employee = await EmployeeEntity.save(assignedEmployees)

    // TODO: return all employees
    return {
      data: {
        employeeID: employee[0].id,
        equipment: equipment,
      },
    }
  }

  public async unassign(opts: EquipmentUnassignRequest): Promise<EquipmentUnassignResponse> {
    const driver = await EmployeeEntity.findOne({ id: opts.employeeID }).catch(
      (err: QueryError) => err,
    )
    if (driver instanceof Error) {
      return {
        errors: [
          { message: 'Error finding driver' },
          { message: driver.name + '\n' + driver.message },
        ],
      }
    } else if (!driver) {
      return { errors: [{ message: 'No matching driver found' }] }
    }

    // TODO: fix this not actually removing equipment reference

    // @ts-expect-error idc
    driver.equipment = null
    const result = await driver.save().catch((err: QueryError) => err)
    if (result instanceof Error) {
      return {
        errors: [
          { message: 'Error saving unassignment' },
          { message: result.name + '\n' + result.message },
        ],
      }
    }

    return {
      data: {
        id: driver.id,
      },
    }
  }
}
