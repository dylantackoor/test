import { hash, compare } from 'bcrypt'
import { createTransport } from 'nodemailer'
import jwt from 'jsonwebtoken'
import { QueryError } from 'mysql2'
import {
  AuthCreateRequest,
  AuthCreateResponse,
  AuthLoginRequest,
  AuthLoginResponse,
  AuthPatchRequest,
  AuthPatchResponse,
  AuthResetConfirmRequest,
  AuthResetInitRequest,
  AuthResetResponse,
  Employee,
  Carrier,
  Broker,
  Registration,
} from '@bigrig/interfaces'

import { UserEntity } from '../entities'
import { CarrierService } from './Carrier.service'
import { BrokerService } from './Broker.service'
import { EmployeeService } from './Employee.service'
import { RegistrationService } from './Registration.service'

// TODO: load these from env.ts
const EMAIL_HOST = 'smtp.ethereal.email'
const EMAIL_PORT = 587
const EMAIL_USER = 'doris.bechtelar@ethereal.email'
const EMAIL_PASSWORD = 'PHTgYcRRGbBtNftegw'
const CLIENT_DOMAIN = 'https://bigrig.dev'

const BrokerClient = new BrokerService()
const CarrierClient = new CarrierService()
const DriverClient = new EmployeeService()
const SetupsClient = new RegistrationService()

// TODO: move to own module
const transporter = createTransport({
  host: EMAIL_HOST,
  port: EMAIL_PORT,
  auth: {
    user: EMAIL_USER,
    pass: EMAIL_PASSWORD,
  },
})

export class AuthService {
  private async getAuthState(): Promise<{
    drivers: Employee[]
    carriers: Carrier[]
    setups: Registration[]
    brokers: Broker[]
  }> {
    const { data: brokers, errors: brokerErrors } = await BrokerClient.find({})
    const { data: carriers, errors: carrierErrors } = await CarrierClient.get({})
    const { data: drivers, errors: driverErrors } = await DriverClient.findAll({})
    const { data: setups, errors: setupErrors } = await SetupsClient.getAll({})

    if (carrierErrors || brokerErrors || setupErrors || driverErrors) {
      console.error('whooops')
      throw new Error()
    }

    return {
      drivers: drivers!,
      carriers: carriers!,
      setups: setups!,
      brokers: brokers!,
    }
  }

  public async register(signupParams: AuthCreateRequest): Promise<AuthCreateResponse> {
    // Verify matching password confirmation
    if (signupParams.password !== signupParams.confirmation) {
      return {
        errors: [
          {
            message: 'Password mismatch',
            inputName: 'password',
          },
        ],
      }
    }

    // Confirm email isn't already registered
    const existingUser = await UserEntity.findOne({ email: signupParams.email }).catch(
      (err: QueryError) => err,
    )
    if (existingUser instanceof Error) {
      return {
        errors: [
          {
            message: 'SQL Error',
          },
          {
            message: existingUser.message,
          },
        ],
      }
    }
    if (existingUser) {
      return {
        errors: [
          {
            message: 'Email already registered',
            inputName: 'email',
          },
        ],
      }
    }

    // Create New User
    const user = new UserEntity()
    user.email = signupParams.email
    // TODO: run class validator here?
    user.password = await hash(signupParams.password, 10)

    const token = Buffer.from(`${user.email}:${signupParams.password}`).toString('base64')

    // TODO: confirm this type when i get debugger setup
    const result = await user.save().catch((err: QueryError) => err)
    if (result instanceof Error) {
      return {
        errors: [
          {
            message: 'Error saving new user',
          },
        ],
      }
    }

    transporter
      .sendMail({
        from: EMAIL_USER,
        to: user.email,
        subject: 'bigrig Registration',
        html: `
          <p>Hey ${user.email},</p>
          <p>Your registered for bigrig!</p>
        `,
      })
      .catch((err: Error) => console.error(err))

    const authState = await this.getAuthState()

    const response: AuthCreateResponse = {
      data: {
        user: { id: result.id, email: user.email },
        token,
        ...authState,
      },
    }

    return response
  }

  public async login(loginQuery: AuthLoginRequest): Promise<AuthLoginResponse> {
    try {
      const { email, password: passwordQuery } = loginQuery
      const existingUser = await UserEntity.findOne(
        { email },
        { select: ['id', 'email', 'password'] },
      )
      if (!existingUser) {
        return {
          errors: [
            {
              message: 'Account/Password combination not found',
            },
          ],
        }
      }

      const passwordMatch = await compare(passwordQuery, existingUser.password)
      if (!passwordMatch) {
        return {
          errors: [
            {
              message: 'Account/Password combination not found',
            },
          ],
        }
      }

      const token = Buffer.from(`${existingUser.email}:${passwordQuery}`).toString('base64')

      const authState = await this.getAuthState()

      const responseData: AuthLoginResponse = {
        data: {
          user: { id: existingUser.id, email: existingUser.email },
          token,
          ...authState,
        },
      }

      return responseData
    } catch (error) {
      return {
        errors: [
          {
            message: 'Error checking for existing user or profile',
          },
          {
            message: error.message,
          },
        ],
      }
    }
  }

  public async updatePassword(req: AuthPatchRequest): Promise<AuthPatchResponse> {
    if (req.body.confirmation !== req.body.password) {
      return {
        errors: [{ message: 'Password mismatch' }],
      }
    }

    // TODO: return password mismatch errors
    const encryptedPassword = await hash(req.body.password, 10).catch((err: Error) => err)
    if (encryptedPassword instanceof Error) {
      return {
        errors: [{ message: 'Password hash error' }],
      }
    }

    const userEntity = new UserEntity()
    userEntity.id = req.user.id
    userEntity.password = encryptedPassword

    // TODO: run class validate?

    const saveResult = await userEntity.save().catch((err: QueryError) => err)
    if (saveResult instanceof Error) {
      return {
        errors: [{ message: 'Error saving new password' }],
      }
    }

    const token = Buffer.from(`${userEntity.email}:${userEntity.password}`).toString('base64')

    return {
      data: {
        token,
      },
    }
  }

  public async initiateReset(resetReq: AuthResetInitRequest): Promise<AuthResetResponse> {
    const user = await UserEntity.findOne(
      { email: resetReq.email },
      { select: ['id', 'password', 'created', 'email'] },
    ).catch((err: QueryError) => err)
    if (!user || user instanceof Error) {
      return {
        data: true,
      }
    }

    const resetToken = jwt.sign(
      { userID: user.id },
      `${user.password}-${user.created.toISOString()}`,
      { expiresIn: 3600 },
    )
    const link = `${CLIENT_DOMAIN}/auth/reset/form/?token=${resetToken}&id=${user.id}`

    await transporter.sendMail({
      from: EMAIL_USER,
      to: user.email,
      subject: 'bigrig Password Recovery',
      html: `
        <p>Hey ${user.email},</p>
        <p>We heard that you lost your password. Sorry about that!</p>
        <p>But don’t worry! You can use the following link to reset your password:</p>
        <a href=${link}>${link}</a>
        <p>If you don’t use this link within 1 hour, it will expire.</p>
      `,
    })

    return {
      data: true,
    }
  }

  public async confirmReset(resetReq: AuthResetConfirmRequest): Promise<AuthResetResponse> {
    if (resetReq.password !== resetReq.confirmation) {
      return {
        errors: [{ message: 'password mismatch' }],
      }
    }

    const user = await UserEntity.findOne(
      { id: resetReq.id },
      { select: ['id', 'password', 'created', 'email'] },
    )
    if (!user) {
      return { errors: [{ message: 'invalid user ID' }] }
    }

    const payload = jwt.verify(
      resetReq.token,
      `${user.password}-${user.created.toISOString()}`,
    ) as {
      userID: string
    }
    if (payload.userID === user.id) {
      user.password = await hash(resetReq.password, 10)
      await user.save()
      await transporter.sendMail({
        from: EMAIL_USER,
        to: user.email,
        subject: 'bigrig Password Recovery',
        html: `
          <p>Hey ${user.email},</p>
          <p>Your password has been reset</p>
        `,
      })

      return {
        data: true,
      }
    } else {
      return { errors: [{ message: 'invalid token' }] }
    }
  }

  public async getByCredentials(email: string, password: string): Promise<UserEntity | null> {
    const existingUser = await UserEntity.findOne({ email })
    if (!existingUser) {
      return null
    }

    const passwordMatch = await compare(password, existingUser.password)
    return passwordMatch ? existingUser : null
  }
}
