import { getConnection } from 'typeorm'
import {
  Carrier,
  CarrierCreationRequest,
  CarrierCreationResponse,
  CarrierGetAllResponse,
} from '@bigrig/interfaces'
import { QueryError } from 'mysql2'

import { CarrierEntity, BankAccountEntity, EmployeeEntity } from '../entities'

export class CarrierService {
  public async get(filter: Partial<Carrier>): Promise<CarrierGetAllResponse> {
    const carriers = await CarrierEntity.find({
      where: filter,
      relations: ['employees', 'equipment'],
    }).catch((err: QueryError) => err)
    if (carriers instanceof Error) {
      return {
        errors: [{ message: 'Error finding carriers' }, { message: carriers.message }],
      }
    }

    return {
      data: carriers,
    }
  }

  public async create(opts: CarrierCreationRequest): Promise<CarrierCreationResponse> {
    const newCarrier = new CarrierEntity()
    newCarrier.name = opts.name
    newCarrier.email = opts.email
    newCarrier.avatarURL = opts.avatarURL

    const newEmployees: EmployeeEntity[] = []
    const newBankAccounts: BankAccountEntity[] = []

    const employeesTmp = opts.employees || []
    employeesTmp.map(employeeOpts => {
      const newBankAccount = new BankAccountEntity()
      newBankAccount.accountNumber = employeeOpts.payment.accountNumber
      newBankAccount.accountType = employeeOpts.payment.accountType
      newBankAccount.routingNumber = employeeOpts.payment.routingNumber
      newBankAccount.holderName = employeeOpts.payment.holderName
      newBankAccounts.push(newBankAccount)

      const newEmployee = new EmployeeEntity()
      newEmployee.carrier = newCarrier
      newEmployee.DOT = employeeOpts.DOT
      newEmployee.EIN = employeeOpts.EIN
      newEmployee.MC = employeeOpts.MC
      newEmployee.SCAC = employeeOpts.SCAC
      newEmployee.city = employeeOpts.city
      newEmployee.email = employeeOpts.email
      newEmployee.fax = employeeOpts.fax
      newEmployee.firstName = employeeOpts.firstName
      newEmployee.lastName = employeeOpts.lastName
      newEmployee.payRate = employeeOpts.payRate
      newEmployee.phone = employeeOpts.phone
      newEmployee.state = employeeOpts.state
      newEmployee.street = employeeOpts.street
      newEmployee.zip = employeeOpts.zip
      newEmployee.payment = newBankAccount
      newEmployees.push(newEmployee)
    })

    // commit transaction
    const connection = getConnection()
    const queryRunner = connection.createQueryRunner()
    let response: CarrierCreationResponse
    await queryRunner.startTransaction()
    try {
      await queryRunner.manager.save(newCarrier)
      await queryRunner.manager.save(newBankAccounts)
      await queryRunner.manager.save(newEmployees)

      await queryRunner.commitTransaction()

      response = { data: { employees: newEmployees, ...newCarrier } }
    } catch (err) {
      await queryRunner.rollbackTransaction()
      response = {
        errors: [{ message: 'Error creating carrier' }, { message: err }],
      }
    } finally {
      await queryRunner.release()
    }

    return response
  }
}
