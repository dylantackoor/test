import { Packet, PacketCreationRequest, PacketCreationResponse } from '@bigrig/interfaces'
import { PacketEntity } from '../entities'

export class PacketService {
  public async get(id: number): Promise<Packet | undefined> {
    const matchingPacket = await PacketEntity.findOne({ id })
    return matchingPacket
  }

  public async create(opts: PacketCreationRequest): Promise<PacketCreationResponse> {
    const newPacket = new PacketEntity()
    newPacket.coordinates = opts.coordinates
    newPacket.pdf = opts.pdf
    await newPacket.save()

    return {
      data: true,
    }
  }
}
