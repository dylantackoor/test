import {
  // Broker,
  BrokerCreationRequest,
  BrokerCreationResponse,
  BrokerFindAllRequest,
  BrokerFindAllResponse,
} from '@bigrig/interfaces'
import { QueryError } from 'mysql2'
import { getConnection } from 'typeorm'
import { BrokerEntity, PacketEntity } from '../entities'

export class BrokerService {
  public async find(filter: BrokerFindAllRequest): Promise<BrokerFindAllResponse> {
    const brokers = await BrokerEntity.find({ where: filter }).catch((err: QueryError) => err)
    if (!brokers) {
      return {
        errors: [
          {
            message: 'Could not find brokers',
          },
        ],
      }
    } else if (brokers instanceof Error) {
      return {
        errors: [
          {
            message: 'Error finding brokers',
          },
          {
            message: brokers.message,
          },
        ],
      }
    }

    return {
      data: brokers,
    }
  }

  public async create(opts: BrokerCreationRequest): Promise<BrokerCreationResponse> {
    const newBroker = new BrokerEntity()
    newBroker.email = opts.email
    newBroker.phone = opts.phone
    newBroker.avatarURL = opts.avatarURL
    newBroker.name = opts.name

    const packetTmp = opts.packets || []
    const newPackets = packetTmp.map(packetOpts => {
      const newPacket = new PacketEntity()
      newPacket.broker = newBroker
      newPacket.pdf = ''
      newPacket.coordinates = {
        fields: packetOpts.inputs.map(({ column, position: { page, x1, x2, y1, y2 } }) => ({
          column,
          page,
          x1,
          x2,
          y1,
          y2,
        })),
      }

      return newPacket
    })

    // commit transaction
    const connection = getConnection()
    const queryRunner = connection.createQueryRunner()
    let response: BrokerCreationResponse
    await queryRunner.startTransaction()
    try {
      await queryRunner.manager.save(newBroker)
      if (newPackets.length > 0) await queryRunner.manager.save(newPackets)

      await queryRunner.commitTransaction()

      response = {
        data: {
          brokerID: newBroker.id,
        },
      }
    } catch (err) {
      await queryRunner.rollbackTransaction()
      response = {
        errors: [{ message: 'Error creating broker' }, { message: err }],
      }
    } finally {
      await queryRunner.release()
    }

    return response
  }
}
