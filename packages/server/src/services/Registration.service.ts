// import fs from 'fs'
// import path from 'path'
// import { promisify } from 'util'
// import { PDFDocument, StandardFonts, rgb } from 'pdf-lib'
import {
  JSONResponseError,
  Registration,
  RegistrationConfirmRequest,
  RegistrationConfirmResponse,
  RegistrationCreationRequest,
  RegistrationCreationResponse,
  RegistrationGetAllResponse,
} from '@bigrig/interfaces'
import { QueryError } from 'mysql2'

import {
  BrokerEntity,
  EmployeeEntity,
  // BrokerEntity,
  // CarrierEntity,
  // EmployeeEntity,
  RegistrationEntity,
} from '../entities'
import {
  // BrokerService,
  PacketService,
} from './'

// const writeFile = promisify(fs.writeFile)
// const readFile = promisify(fs.readFile)

// TODO: replace with class-validator
function isValidEmail(email: string): boolean {
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

export class RegistrationService {
  public async get(opts: Partial<Registration>): Promise<Registration | undefined> {
    const matchingRegistration = await RegistrationEntity.findOne(opts, {
      relations: ['packet', 'carrier', 'employee', 'broker'],
    })
    return matchingRegistration
  }

  public async getAll(opts: Partial<Registration>): Promise<RegistrationGetAllResponse> {
    const matchingRegistration = await RegistrationEntity.find({
      where: opts,
      relations: ['packet', 'carrier', 'employee', 'broker'],
    }).catch((err: QueryError) => err)
    if (matchingRegistration instanceof Error) {
      return { errors: [{ message: 'uh ho' }] }
    }

    return {
      data: matchingRegistration,
    }
  }

  public async create(opts: RegistrationCreationRequest): Promise<RegistrationCreationResponse> {
    const errors: JSONResponseError[] = []
    if (!isValidEmail(opts.fromBrokerEmail)) {
      errors.push({ message: 'Invalid From Email' })
    }
    if (!isValidEmail(opts.toEmployeeEmail)) {
      errors.push({ message: 'Invalid To Email' })
    }
    if (errors.length > 0) {
      return { errors }
    }

    const emp = await EmployeeEntity.findOne(
      { email: opts.toEmployeeEmail },
      { relations: ['carrier'] },
    )
    if (!emp) {
      return { errors: [{ message: 'Could not match employee' }] }
    }

    const broker = await BrokerEntity.findOne({ email: opts.fromBrokerEmail })
    if (!broker) {
      return { errors: [{ message: 'Could not match broker' }] }
    }

    const registration = new RegistrationEntity()
    registration.employee = emp
    registration.carrier = emp.carrier
    registration.broker = broker

    const res = await registration.save().catch((err: QueryError) => err)
    if (res instanceof Error) {
      return { errors: [{ message: res.message }] }
    }

    return { data: res }
  }

  /** TODO: update once bucket storage is implemented */
  public async confirm(opts: RegistrationConfirmRequest): Promise<RegistrationConfirmResponse> {
    const PacketClient = new PacketService()
    const packet = await PacketClient.get(opts.packetID)
    if (!packet) {
      return { errors: [{ message: 'error getting packet template' }] }
    }

    // const pdfPath = path.join(__dirname, '..', 'templates/express.pdf')
    // const sampelPdfBytes = await readFile(pdfPath)
    // const pdfDoc = await PDFDocument.load(sampelPdfBytes)

    // const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica)
    // const pages = pdfDoc.getPages()
    // console.log('pages', pages[0].getSize())

    // packet.coordinates.fields.forEach(field => {
    //   const text = opts.answers[field.title] || 'TEST'
    //   pages[field.page].drawText(text, {
    //     x: 275,
    //     y: 720,
    //     size: 12,
    //     color: rgb(0.95, 0.1, 0.1),
    //     font: helveticaFont,
    //   })
    // })

    // const pdfBytes = await pdfDoc.save()

    // const outDir = path.join(__dirname, '..', `templates/${packet.id}.pdf`)
    // await writeFile(outDir, pdfBytes)

    await RegistrationEntity.update({ id: opts.registrationID }, { confirmed: new Date() })

    return { data: true }
  }
}
