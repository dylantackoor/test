import { Router } from 'express'
import swaggerUi from 'swagger-ui-express'

const swaggerFilePath = '../../build/swagger.json'

export const swaggerRouter = Router()

swaggerRouter.use(swaggerUi.serve)
swaggerRouter.get('/', async (req, res) => {
  const page = swaggerUi.generateHTML(await import(swaggerFilePath))
  return res.send(page)
})
