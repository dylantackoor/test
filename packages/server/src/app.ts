import express, { urlencoded, json, NextFunction, Request, Response } from 'express'
import morgan from 'morgan'
import cors from 'cors'
import cookieParser from 'cookie-parser'
import { ValidateError } from '@tsoa/runtime'

import { RegisterRoutes } from './routes'
import { healthCheckRouter, swaggerRouter } from './routes.custom'

const { FRONTEND_URL, SALT } = process.env

export const app = express()

app.disable('x-powered-by')
app.use(morgan('dev'))
app.use(json())
app.use(urlencoded({ extended: true }))
app.use(cookieParser(SALT))
app.use(
  cors({
    origin: FRONTEND_URL,
    credentials: true,
  }),
)

RegisterRoutes(app)
app.use('/healthcheck', healthCheckRouter)
app.use('/docs', swaggerRouter)
app.use(
  (
    err: Error | ValidateError,
    req: Request,
    res: Response,
    next: NextFunction,
  ): Response | void => {
    if (err instanceof ValidateError) {
      console.warn(`Caught Validation Error for ${req.path}:`, err.fields)
      return res.status(422).json({
        message: 'Validation Failed',
        details: err?.fields,
      })
    } else if (err instanceof Error) {
      console.error(err)
      return res.status(500).json({
        message: 'Internal Server Error',
      })
    }

    next()
  },
)
