import { Request } from 'express'
import { User } from '@bigrig/interfaces'
import { AuthService } from '../services'

const AuthClient = new AuthService()

export async function expressAuthentication(
  request: Request,
  securityName: string,
  scopes?: string[],
): Promise<User> {
  if (securityName === 'basic' && request.headers.authorization?.includes('Basic ')) {
    const token = request.headers.authorization.split('Basic ')[1]
    const [email, password] = Buffer.from(token, 'base64').toString('utf8').split(':')
    const user = await AuthClient.getByCredentials(email, password)
    const result = user ? Promise.resolve(user) : Promise.reject({})
    return result
  }

  return Promise.reject({})
}
