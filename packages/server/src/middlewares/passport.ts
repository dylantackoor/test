// TODO: Actually use this :)

import { Strategy } from 'passport-local'
import { UserEntity } from '../entities'

export const localStrategy = new Strategy(async (username, password, cb) => {
  const user = await UserEntity.findOne(
    { email: username, password },
    { select: ['email', 'password'] },
  ).catch((error: Error) => error)

  if (user instanceof Error) {
    return cb(user)
  } else if (!user || user.password !== password) {
    return cb(null, false)
  } else {
    return cb(null, user)
  }
})
