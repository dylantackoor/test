import { Body, Controller, Post, Route, Request, Security, Tags, Example } from 'tsoa'
import express from 'express'
import multer from 'multer'
import {
  BrokerCreationRequest,
  BrokerCreationResponse,
  BrokerFindRequest,
  BrokerFindAllResponse,
} from '@bigrig/interfaces'

import { BrokerService } from '../services'

const BrokerClient = new BrokerService()

@Tags('Broker')
@Route('brokers')
@Security('basic')
export class BrokerController extends Controller {
  @Post('/find')
  public async get(
    @Body()
    body: BrokerFindRequest,
  ): Promise<BrokerFindAllResponse> {
    return await BrokerClient.find(body)
  }

  /** Creates a new Broker */
  @Post('/')
  @Example<BrokerCreationResponse>({
    data: {
      brokerID: 1,
    },
  })
  public async create(@Body() body: BrokerCreationRequest): Promise<BrokerCreationResponse> {
    this.setStatus(201)
    return await BrokerClient.create(body)
  }

  @Post('/upload')
  public async uploadImageFile(@Request() request: express.Request): Promise<any> {
    console.log(request.body)
    await this.handleFile(request, 'pdfFile', 'pdf', './uploads')
    return {
      data: true,
    }
  }

  private async handleFile(
    request: express.Request,
    requestField: string,
    filterExt: string,
    storePath: string,
  ): Promise<any> {
    const uploadOptions: multer.Options = {
      storage: multer.diskStorage({
        destination: (req, file, callback) => {
          callback(null, `${storePath}/`)
        },
        filename: (req, file, callback) => {
          callback(null, file.originalname)
        },
      }),
      fileFilter: (req, file, callback) => {
        callback(null, true)
      },
    }

    const upload = multer(uploadOptions)
    const multerSingle = upload.single(requestField)

    // @ts-expect-error idc
    multerSingle(request, {}, async (error: Error) => {
      if (error) {
        return Promise.reject(error)
      }

      return Promise.resolve()
    })
  }
}
