import { Body, Controller, Example, Post, Route, Tags, Security, NoSecurity } from 'tsoa'
import {
  RegistrationConfirmRequest,
  RegistrationConfirmResponse,
  RegistrationCreationRequest,
  RegistrationCreationResponse,
  RegistrationGetAllRequest,
  RegistrationGetAllResponse,
} from '@bigrig/interfaces'

import { RegistrationService } from '../services'

const RegistrationClient = new RegistrationService()

@Tags('Registration')
@Route('registrations')
@Security('basic')
export class RegistrationController extends Controller {
  /**
   * Logs an incoming registration.
   * Ideally would also take an `attachmentURL` string.
   * Called by @bigrig/gcp
   */
  @Post('/')
  @NoSecurity()
  public async create(
    @Body() postData: RegistrationCreationRequest,
  ): Promise<RegistrationCreationResponse> {
    const res = await RegistrationClient.create(postData)
    return res
  }

  /** Retrieves all registrations */
  @Post('/find')
  @NoSecurity()
  public async getAll(
    @Body() body: RegistrationGetAllRequest,
  ): Promise<RegistrationGetAllResponse> {
    const res = await RegistrationClient.getAll(body)
    return res
  }

  /** Renders new PDF with answers + sets confirmation date*/
  @Example<RegistrationConfirmResponse>({
    data: true,
  })
  @Post('/confirm')
  public async confirm(
    @Body() formData: RegistrationConfirmRequest,
  ): Promise<RegistrationConfirmResponse> {
    const res = await RegistrationClient.confirm(formData)
    return res
  }
}
