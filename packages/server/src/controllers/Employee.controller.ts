import {
  Body,
  Controller,
  Post,
  Route,
  Tags,
  Security,
  // Example
} from 'tsoa'
import {
  EmployeeCreationRequest,
  EmployeeCreationResponse,
  EmployeeFindAllRequest,
  EmployeeFindAllResponse,
  EmployeeFindRequest,
  EmployeeFindResponse,
} from '@bigrig/interfaces'

import { EmployeeService } from '../services'

@Tags('Employee')
@Route('employees')
@Security('basic')
export class EmployeeController extends Controller {
  /** Retrieves employees */
  // @Example<EmployeeGetAllResponse>({
  //   data: [
  //     {
  //       id: 1,
  //       email: 'test@bigrig.dev',
  //       DOT: 0,
  //       EIN: 0,
  //       MC: 0,
  //     },
  //   ],
  // })
  @Post('/find')
  public async getUser(@Body() body: EmployeeFindRequest): Promise<EmployeeFindResponse> {
    return new EmployeeService().find(body)
  }

  /** Retrieves all Equipment */
  @Post('/findAll')
  public async findAll(@Body() body: EmployeeFindAllRequest): Promise<EmployeeFindAllResponse> {
    return new EmployeeService().findAll(body)
  }

  /** Creates a new employee */
  // @Example<EmployeeCreationResponse>({
  //   data: {
  //     DOT: 0,
  //   },
  // })
  @Post('/')
  public async create(
    @Body() createQuery: EmployeeCreationRequest,
  ): Promise<EmployeeCreationResponse> {
    this.setStatus(201)
    return await new EmployeeService().create(createQuery)
  }
}
