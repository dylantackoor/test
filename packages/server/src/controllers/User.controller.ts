import { Controller, Example, Get, Request, Route, Security, Tags } from 'tsoa'
import { UserGetResponse } from '@bigrig/interfaces'

import { UserService } from '../services'
import { AuthenticatedRequest } from '../types'

@Tags('User')
@Route('users')
export class UsersController extends Controller {
  /** Retrieves the details of a currently logged in user. */
  @Example<UserGetResponse>({
    data: {
      id: 'b5d9cf7c-296c-4ef3-8024-190b1e4e69d8',
      email: 'test@bigrig.dev',
    },
  })
  @Security('basic')
  @Get('/')
  public async getUser(@Request() request: AuthenticatedRequest): Promise<UserGetResponse> {
    return new UserService().find(request.user.id)
  }
}
