import {
  Request,
  Body,
  Controller,
  Example,
  Post,
  Route,
  Security,
  SuccessResponse,
  Tags,
} from 'tsoa'
import {
  AuthCreateRequest,
  AuthCreateResponse,
  AuthLoginRequest,
  AuthLoginResponse,
  AuthResetInitRequest,
  AuthResetResponse,
  AuthResetConfirmRequest,
  AuthPatchRequest,
  AuthPatchResponse,
} from '@bigrig/interfaces'

import { AuthService } from '../services'

const AuthClient = new AuthService()

const authStateExample = {
  user: {
    id: 'b5d9cf7c-296c-4ef3-8024-190b1e4e69d8',
    email: 'example@bigrig.dev',
  },
  token: '',
  setups: [],
  carriers: [],
  brokers: [],
  drivers: [],
}

@Tags('Auth')
@Route('auth')
export class AuthController extends Controller {
  /**  Creates a user given user info + minimum user profile info */
  @Example<AuthCreateResponse>({
    data: authStateExample,
  })
  @SuccessResponse('201', 'Created')
  @Post('/register')
  public async create(@Body() requestBody: AuthCreateRequest): Promise<AuthCreateResponse> {
    this.setStatus(201)
    return await AuthClient.register(requestBody)
  }

  @Post('/update')
  @Security('basic')
  public async update(@Request() request: AuthPatchRequest): Promise<AuthPatchResponse> {
    return await AuthClient.updatePassword(request)
  }

  /** Returns a user given correct login details. */
  @Example<AuthLoginResponse>({
    data: authStateExample,
  })
  @SuccessResponse('200', 'OK')
  @Post('/login')
  public async login(@Body() body: AuthLoginRequest): Promise<AuthLoginResponse> {
    this.setStatus(201)
    return await AuthClient.login(body)
  }

  /** Sends a password reset email if the user exists */
  @Post('/reset/initiate')
  public async initiateReset(@Body() body: AuthResetInitRequest): Promise<AuthResetResponse> {
    return await AuthClient.initiateReset(body)
  }

  /** Updates a user's password if given a valid token */
  @Post('/reset/confirm')
  public async confirmReset(@Body() body: AuthResetConfirmRequest): Promise<AuthResetResponse> {
    return await AuthClient.confirmReset(body)
  }
}
