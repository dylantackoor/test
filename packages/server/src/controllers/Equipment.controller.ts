import { Body, Controller, Post, Route, Tags, Security } from 'tsoa'
import {
  EquipmentCreationRequest,
  EquipmentCreationResponse,
  EquipmentFindRequest,
  EquipmentFindResponse,
  EquipmentFindAllRequest,
  EquipmentFindAllResponse,
  EquipmentAssignRequest,
  EquipmentAssignResponse,
  EquipmentUnassignRequest,
  EquipmentUnassignResponse,
} from '@bigrig/interfaces'

import { EquipmentService } from '../services'

const EquipmentClient = new EquipmentService()

@Tags('Equipment')
@Route('equipment')
@Security('basic')
export class EquipmentController extends Controller {
  /** Retrieves Equipment */
  @Post('/find')
  public async find(@Body() body: EquipmentFindRequest): Promise<EquipmentFindResponse> {
    return await EquipmentClient.find(body)
  }

  /** Retrieves all Equipment */
  @Post('/findAll')
  public async findAll(@Body() body: EquipmentFindAllRequest): Promise<EquipmentFindAllResponse> {
    return await EquipmentClient.findAll(body)
  }

  /** adds Equipment by ID to Employees by employeeIDs */
  @Post('/assign')
  public async assign(@Body() body: EquipmentAssignRequest): Promise<EquipmentAssignResponse> {
    return await EquipmentClient.assign(body)
  }

  /** Removes equipment from single driver by driver ID */
  @Post('/unassign')
  public async unassign(
    @Body() body: EquipmentUnassignRequest,
  ): Promise<EquipmentUnassignResponse> {
    return await EquipmentClient.unassign(body)
  }

  /** Creates equipment */
  @Post('/')
  public async create(@Body() body: EquipmentCreationRequest): Promise<EquipmentCreationResponse> {
    this.setStatus(201)
    return await EquipmentClient.create(body)
  }
}
