import { Body, Controller, Post, Route, Tags, Security } from 'tsoa'
import {
  // Employee,
  CarrierCreationRequest,
  CarrierCreationResponse,
  CarrierGetAllResponse,
} from '@bigrig/interfaces'

import { CarrierService } from '../services'

const CarrierClient = new CarrierService()

@Tags('Carrier')
@Route('carriers')
@Security('basic')
export class CarrierController extends Controller {
  /** Retrieves carriers */
  // @Example<CarrierGetAllResponse>({
  //   data: [
  //     {
  //       id: 1,
  //       email: 'test@bigrig.dev',
  //       DOT: 0,
  //       EIN: 0,
  //       MC: 0,
  //     },
  //   ],
  // })
  @Post('/find')
  public async getUser(): Promise<CarrierGetAllResponse> {
    return await CarrierClient.get({})
  }

  /** Creates a new employee */
  @Post('/')
  public async create(
    @Body() createQuery: CarrierCreationRequest,
  ): Promise<CarrierCreationResponse> {
    this.setStatus(201)
    return await new CarrierService().create(createQuery)
  }
}
