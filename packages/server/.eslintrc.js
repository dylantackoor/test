module.exports = {
  root: true,
  extends: ['@bigrig/eslint-config-bigrig'],
  ignorePatterns: ['build/*', 'dist/*'],
}
