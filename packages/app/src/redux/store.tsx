import { configureStore, combineReducers, ThunkAction, Action } from '@reduxjs/toolkit'
import { useDispatch } from 'react-redux'
import logger from 'redux-logger'
import storage from 'redux-persist/lib/storage'
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist'

import { authReducer } from './features/auth/authSlice'
import { brokerReducer } from './features/brokers/brokerSlice'
import { carrierReducer } from './features/carriers/carrierSlice'
import { driverReducer } from './features/drivers/driverSlice'
import { setupReducer } from './features/setups/setupsSlice'
import { uiReducer } from './features/ui/uiSlice'
import { userReducer } from './features/user/userSlice'

const rootReducer = combineReducers({
  auth: authReducer,
  brokers: brokerReducer,
  carriers: carrierReducer,
  drivers: driverReducer,
  setups: setupReducer,
  ui: uiReducer,
  user: userReducer,
})

const persistedReducer = persistReducer(
  {
    key: 'root',
    storage,
  },
  rootReducer,
)

export const store = configureStore({
  devTools: true,
  reducer: persistedReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }).concat(logger),
})

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>

export const persistor = persistStore(store)
export const useAppDispatch = () => useDispatch<AppDispatch>()
