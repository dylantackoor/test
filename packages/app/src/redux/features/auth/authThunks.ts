import { createAsyncThunk } from '@reduxjs/toolkit'
import {
  JSONResponseError,
  AuthLoginRequest,
  AuthLoginResponse,
  AuthCreateRequest,
  AuthCreateResponse,
} from '@bigrig/interfaces'

import { callApi } from '../../api'
import { authActions } from './authSlice'
import { brokerActions } from '../brokers/brokerSlice'
import { carrierActions } from '../carriers/carrierSlice'
import { driverActions } from '../drivers/driverSlice'
import { setupActions } from '../setups/setupsSlice'
import { userActions } from '../user/userSlice'

export const logout = createAsyncThunk('USER/LOGOUT', async (_, { dispatch }) => {
  dispatch(authActions.unset())
  dispatch(brokerActions.unset())
  dispatch(carrierActions.unset())
  dispatch(driverActions.unset())
  dispatch(setupActions.unset())
  dispatch(userActions.unset())
})

/** Given credentials, retrieves/sets auth, user, and app info */
export const login = createAsyncThunk(
  'USER/LOGIN',
  async (opts: AuthLoginRequest, { rejectWithValue, dispatch }) => {
    try {
      const res = await callApi<AuthLoginResponse>('/auth/login', {
        method: 'POST',
        body: JSON.stringify(opts),
        headers: {
          'content-type': 'application/json;charset=UTF-8',
        },
      })

      if (res.errors) {
        const errs: JSONResponseError[] = res.errors
        rejectWithValue(errs)
      }

      const { token, drivers, brokers, carriers, setups, user } = res.data!

      dispatch(authActions.set(token))
      dispatch(brokerActions.set(brokers))
      dispatch(carrierActions.set(carriers))
      dispatch(driverActions.set(drivers))
      dispatch(setupActions.set(setups))
      dispatch(userActions.set(user))
    } catch (error) {
      // TODO: replace with proper logging solution
      console.error(error)
      return rejectWithValue(error.meta)
    }
  },
)

/** Register user to system */
export const register = createAsyncThunk(
  'USER/REGISTER',
  async (opts: AuthCreateRequest, { rejectWithValue }) => {
    try {
      const res = await callApi<AuthCreateResponse>('/auth/register', {
        method: 'POST',
        body: JSON.stringify(opts),
        headers: {
          'content-type': 'application/json;charset=UTF-8',
        },
      })

      if (res.errors) {
        const errs: JSONResponseError[] = res.errors
        rejectWithValue(errs)
      }

      return res.data
    } catch (error) {
      // TODO: replace with proper logging solution
      console.error(error)
      return rejectWithValue(error.meta)
    }
  },
)
