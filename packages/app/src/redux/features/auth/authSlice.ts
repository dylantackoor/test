import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { RootState } from '../../store'

export interface Auth2State {
  token: string
}

const initialState: Auth2State = {
  token: '',
}

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    set: (state, action: PayloadAction<string>) => {
      state.token = action.payload
    },
    unset: state => {
      state.token = ''
    },
  },
})

export const authReducer = authSlice.reducer
export const authActions = authSlice.actions
export const authSelector = (state: RootState): Auth2State => state.auth
