import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Carrier } from '@bigrig/interfaces'

import { RootState } from '../../store'

export interface CarrierState {
  loading: boolean
  carriers: Carrier[]
}

const initialState: CarrierState = {
  loading: false,
  carriers: [],
}

export const carrierSlice = createSlice({
  name: 'carriers',
  initialState,
  reducers: {
    set: (state, action: PayloadAction<Carrier[]>) => {
      state.carriers = action.payload
      state.loading = false
    },
    unset: state => {
      state.loading = false
      state.carriers = []
    },
  },
})

export const carrierReducer = carrierSlice.reducer
export const carrierActions = carrierSlice.actions
export const carrierSelector = (state: RootState): CarrierState => state.carriers
