import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../../store'

export interface UiState {
  searchTarget: string
  sidebarOpen: boolean
}

const initialState: UiState = {
  searchTarget: '',
  sidebarOpen: false,
}

export const uiSlice = createSlice({
  name: 'ui',
  initialState,
  reducers: {
    toggleSidebar: state => {
      state.sidebarOpen = !state.sidebarOpen
    },
    setSearch: (state, action: PayloadAction<string>) => {
      state.searchTarget = action.payload
    },
  },
})

export const uiReducer = uiSlice.reducer
export const uiActions = uiSlice.actions
export const uiSelector = (state: RootState): UiState => state.ui
