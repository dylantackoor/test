import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Employee as Driver } from '@bigrig/interfaces'

import { RootState } from '../../store'
import { assignEquipment, unassignEquipment } from './driverThunk'

export interface DriverState {
  loading: boolean
  drivers: Driver[]
}

const initialState: DriverState = {
  loading: false,
  drivers: [],
}

export const driverSlice = createSlice({
  name: 'drivers',
  initialState,
  reducers: {
    set: (state, action: PayloadAction<Driver[]>) => {
      state.drivers = action.payload
      state.loading = false
    },
    unset: state => {
      state.loading = false
      state.drivers = []
    },
  },
  extraReducers: builder => {
    // Assign
    builder.addCase(assignEquipment.pending, state => {
      state.loading = true
    })
    builder.addCase(assignEquipment.fulfilled, (state, action) => {
      const { employeeID, equipment } = action.payload!
      const driverIndex = state.drivers.findIndex(driver => driver.id === employeeID)

      state.drivers[driverIndex].equipment = equipment
      state.loading = false
    })
    builder.addCase(assignEquipment.rejected, (state, action) => {
      state.loading = false
      // if (action.payload) {
      //   state.errors = action.payload as JSONResponseError[]
      // }
    })

    // Unassign
    builder.addCase(unassignEquipment.pending, state => {
      state.loading = true
    })
    builder.addCase(unassignEquipment.fulfilled, (state, action) => {
      const { id } = action.payload!
      const driverIndex = state.drivers.findIndex(driver => driver.id === id)

      state.drivers[driverIndex].equipment = undefined
      state.loading = false
    })
    builder.addCase(unassignEquipment.rejected, (state, action) => {
      state.loading = false
      // if (action.payload) {
      //   state.errors = action.payload as JSONResponseError[]
      // }
    })
  },
})

export const driverReducer = driverSlice.reducer
export const driverActions = driverSlice.actions
export const driverSelector = (state: RootState): DriverState => state.drivers
