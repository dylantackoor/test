import { createAsyncThunk } from '@reduxjs/toolkit'
import {
  EquipmentAssignRequest,
  EquipmentAssignResponse,
  EquipmentUnassignRequest,
  EquipmentUnassignResponse,
  JSONResponseError,
} from '@bigrig/interfaces'

import { RootState } from '../../store'
import { callApi } from '../../api'

/** Assigns driver to equipment */
export const assignEquipment = createAsyncThunk(
  'EQUIPMENT/ASSIGN',
  async (opts: EquipmentAssignRequest, { rejectWithValue, getState }) => {
    try {
      const state = getState() as RootState

      const res = await callApi<EquipmentAssignResponse>('/equipment/assign', {
        method: 'POST',
        body: JSON.stringify(opts),
        headers: {
          'content-type': 'application/json;charset=UTF-8',
          Authorization: `Basic ${state.auth.token}`,
        },
      })

      if (res.errors) {
        const errs: JSONResponseError[] = res.errors
        rejectWithValue(errs)
      }

      return res.data
    } catch (error) {
      // TODO: replace with proper logging solution
      console.error(error)
      return rejectWithValue(error.meta)
    }
  },
)

/** Revmoes driver from equipment */
export const unassignEquipment = createAsyncThunk(
  'EQUIPMENT/UNASSIGN',
  async (opts: EquipmentUnassignRequest, { rejectWithValue, getState }) => {
    try {
      const state = getState() as RootState

      const res = await callApi<EquipmentUnassignResponse>('/equipment/unassign', {
        method: 'POST',
        body: JSON.stringify(opts),
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Basic ${state.auth.token}`,
        },
      })

      if (res.errors) {
        const errs: JSONResponseError[] = res.errors
        rejectWithValue(errs)
      }

      return res.data
    } catch (error) {
      // TODO: replace with proper logging solution
      console.error(error)
      return rejectWithValue(error.meta)
    }
  },
)
