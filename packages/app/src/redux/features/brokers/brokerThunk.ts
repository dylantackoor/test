import { createAsyncThunk } from '@reduxjs/toolkit'
import {
  BrokerCreationRequest,
  BrokerCreationResponse,
  JSONResponseError,
} from '@bigrig/interfaces'

import { RootState } from '../../store'
import { callApi } from '../../api'
import { brokerActions } from '../brokers/brokerSlice'

export const addBroker = createAsyncThunk(
  'broker/create',
  async (opts: BrokerCreationRequest, { dispatch, getState, rejectWithValue }) => {
    try {
      const state = getState() as RootState

      const res = await callApi<BrokerCreationResponse>('/brokers', {
        method: 'POST',
        body: JSON.stringify(opts),
        headers: {
          'content-type': 'application/json;charset=UTF-8',
          Authorization: `Basic ${state.auth.token}`,
        },
      })

      if (res.errors) {
        const errs: JSONResponseError[] = res.errors
        console.error(errs)
        rejectWithValue(errs)
      }

      const newBroker = {
        id: res.data!.brokerID,
        email: opts.email,
        name: opts.name,
        phone: opts.phone,
        avatarURL:
          opts.avatarURL || 'https://www.gravatar.com/avatar/c30de8aad220b65c3d07222b1ee835d8',
      }

      dispatch(brokerActions.add(newBroker))

      return Promise.resolve()
    } catch (error) {
      // TODO: replace with proper logging solution
      console.error(error)
      return rejectWithValue(error.meta)
    }
  },
)

export const saveFile = createAsyncThunk(
  'broker/savePDF',
  async (opts: { pdfFile: File }, { getState }) => {
    const state = getState() as RootState

    const formData = new FormData()
    formData.append('pdfFile', opts.pdfFile, opts.pdfFile.name)

    await callApi('/brokers/upload', {
      method: 'POST',
      body: formData,
      headers: {
        Authorization: `Basic ${state.auth.token}`,
      },
    })
  },
)
