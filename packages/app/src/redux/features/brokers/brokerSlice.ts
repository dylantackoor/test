import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Broker } from '@bigrig/interfaces'

import { RootState } from '../../store'

type BrokeFE = Omit<Broker, 'packets'>

export interface BrokerState {
  loading: boolean
  brokers: BrokeFE[]
}

const initialState: BrokerState = {
  loading: false,
  brokers: [],
}

export const brokerSlice = createSlice({
  name: 'brokers',
  initialState,
  reducers: {
    set: (state, action: PayloadAction<BrokeFE[]>) => {
      state.brokers = action.payload
      state.loading = false
    },
    unset: state => {
      state.loading = false
      state.brokers = []
    },
    add: (state, action: PayloadAction<BrokeFE>) => {
      state.brokers = [...state.brokers, action.payload]
      state.loading = false
    },
  },
})

export const brokerReducer = brokerSlice.reducer
export const brokerActions = brokerSlice.actions
export const brokerSelector = (state: RootState): BrokerState => state.brokers
