import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { User } from '@bigrig/interfaces'

import { RootState } from '../../store'

export type UserState = Omit<User, 'password'>

const initialState: UserState = {
  id: '',
  email: '',
}

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    set: (state, action: PayloadAction<UserState>) => {
      state.id = action.payload.id
      state.email = action.payload.email
    },
    unset: state => {
      state.id = ''
      state.email = ''
    },
  },
})

export const userReducer = userSlice.reducer
export const userActions = userSlice.actions
export const userSelector = (state: RootState): UserState => state.user
