import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Registration as Setup } from '@bigrig/interfaces'

import { RootState } from '../../store'

export interface SetupState {
  loading: boolean
  setups: Setup[]
}

const initialState: SetupState = {
  loading: false,
  setups: [],
}

export const setupSlice = createSlice({
  name: 'setups',
  initialState,
  reducers: {
    set: (state, action: PayloadAction<Setup[]>) => {
      state.setups = action.payload
      state.loading = false
    },
    unset: state => {
      state.loading = false
      state.setups = []
    },
  },
})

export const setupReducer = setupSlice.reducer
export const setupActions = setupSlice.actions
export const setupSelector = (state: RootState): SetupState => state.setups
