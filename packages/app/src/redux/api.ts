import { JSONResponse } from '@bigrig/interfaces'

export async function callApi<T>(path: string, opts: RequestInit): Promise<T> {
  const url = `${import.meta.env.VITE_API_BASE_URL}${path}`

  const data = await fetch(url, opts)
  const res: T = await data.json().catch(
    (fetchErr: Error): JSONResponse<T> => ({
      errors: [
        {
          message: fetchErr.message,
        },
      ],
    }),
  )

  return res
}
