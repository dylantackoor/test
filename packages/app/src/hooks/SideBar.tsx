import React, {
  useState,
  useEffect,
  useMemo,
  FunctionComponent,
  createContext,
  useContext,
} from 'react'

interface ISidebarContext {
  isSidebarOpen: boolean
  closeSidebar: () => void
  toggleSidebar: () => void
}

// @ts-expect-error idk
export const SidebarContext = createContext<ISidebarContext>({})
export const useSidebarContext = (): ISidebarContext => useContext(SidebarContext)
export const SidebarProvider: FunctionComponent = ({ children }) => {
  const [isSidebarOpen, setIsSidebarOpen] = useState(false)

  const toggleSidebar = () => setIsSidebarOpen(!isSidebarOpen)
  const closeSidebar = () => setIsSidebarOpen(false)

  useEffect(() => {
    return
    // TODO:
    // router.events.on('routeChangeStart', closeSidebar)
    // return () => router.events.off('routeChangeStart', closeSidebar)
  })

  const value = useMemo(
    () => ({
      isSidebarOpen,
      toggleSidebar,
      closeSidebar,
    }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isSidebarOpen],
  )

  return <SidebarContext.Provider value={value}>{children}</SidebarContext.Provider>
}
