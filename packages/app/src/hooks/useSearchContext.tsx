import React, {
  useState,
  FunctionComponent,
  createContext,
  Dispatch,
  SetStateAction,
  useContext,
} from 'react'

interface ISearchContext {
  searchTarget: string
  setSearchTarget: Dispatch<SetStateAction<string>>
}

// @ts-expect-error expects default values but nah
export const SearchContext = createContext<ISearchContext>({})
export const useSearchContext = (): ISearchContext => useContext(SearchContext)
export const SearchProvider: FunctionComponent = ({ children }) => {
  const [searchTarget, setSearchTarget] = useState('')

  const value = {
    searchTarget,
    setSearchTarget,
  }

  return <SearchContext.Provider value={value}>{children}</SearchContext.Provider>
}
