import React, { useContext, FunctionComponent, useEffect } from 'react'
import { useNavigate } from '@reach/router'

import { Sidebar, Header } from '../components'
import { SidebarContext } from '../hooks/SideBar'
import { authSelector } from '../redux/features/auth/authSlice'
import { useAppSelector } from '../redux/hooks'

export const AppLayout: FunctionComponent = ({ children }) => {
  const { isSidebarOpen } = useContext(SidebarContext)

  const { token } = useAppSelector(authSelector)
  const navigate = useNavigate()

  useEffect(() => {
    if (token === '') {
      navigate('/')
    }
  }, [token])

  // useEffect(() => {
  //   closeSidebar()
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [location])

  return (
    <div
      className={`flex h-screen bg-gray-50 dark:bg-gray-900 ${isSidebarOpen && 'overflow-hidden'}`}
    >
      <Sidebar />

      <div className="flex flex-col flex-1 w-full">
        <Header />
        <main className="h-full overflow-y-auto">
          <div className="container grid px-6 mx-auto">{children}</div>
        </main>
      </div>
    </div>
  )
}
