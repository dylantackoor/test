import React, { FunctionComponent } from 'react'
import { Router } from '@reach/router'

import {
  BrokersFormRoute,
  BrokersRoute,
  CarriersRoute,
  DriversRoute,
  LoginRoute,
  NotFoundRoute,
  RegisterRoute,
  SetupsRoute,
} from './routes'

export const App: FunctionComponent = () => (
  <Router>
    <LoginRoute path="/" />
    <RegisterRoute path="/register" />

    <SetupsRoute path="/setups" />

    <DriversRoute path="/drivers" />
    <CarriersRoute path="/carriers" />
    <BrokersRoute path="/brokers" />
    <BrokersFormRoute path="/brokers/new" />

    <NotFoundRoute default />
  </Router>
)
