import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { Windmill } from '@windmill/react-ui'

import './index.css'
import { App } from './App'
import { BigRigWindmillTheme } from './theme'
import { store, persistor } from './redux/store'

// TOOD: merge with redux store and remove
import { SidebarProvider } from './hooks/SideBar'
import { SearchProvider } from './hooks/useSearchContext'

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <SidebarProvider>
          <SearchProvider>
            <Windmill theme={BigRigWindmillTheme}>
              <App />
            </Windmill>
          </SearchProvider>
        </SidebarProvider>
      </PersistGate>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
)
