import React, { FunctionComponent, useState } from 'react'
import { Modal, ModalBody, ModalHeader, ModalFooter, Button } from '@windmill/react-ui'
import { IconContext } from 'react-icons'
import { AiFillDelete } from 'react-icons/ai'

export interface UnassignModalProps {
  driverName: string
  action: any
  equipmentType: string
}

export const UnassignModal: FunctionComponent<UnassignModalProps> = ({
  driverName,
  equipmentType,
  action,
}) => {
  // MODAL
  const [isModalOpen, setIsModalOpen] = useState(false)
  const openModal = () => setIsModalOpen(true)
  const closeModal = () => setIsModalOpen(false)

  const confirmHandler = () => {
    action()
    closeModal()
  }

  return (
    <div className="flex">
      <IconContext.Provider value={{ color: '#FF2247' }}>
        <AiFillDelete className="cursor-pointer" onClick={openModal} />
      </IconContext.Provider>

      <Modal isOpen={isModalOpen} onClose={closeModal}>
        <ModalHeader>Un-assign Modal Confirmation</ModalHeader>
        <ModalBody>
          Remove {driverName} from {equipmentType}?
        </ModalBody>
        <ModalFooter>
          <div>
            <div className="sm:block">
              <Button layout="outline" onClick={closeModal}>
                Cancel
              </Button>
            </div>
            <div className="sm:block">
              <Button onClick={confirmHandler} type="submit">
                Accept
              </Button>
            </div>
          </div>
        </ModalFooter>
      </Modal>
    </div>
  )
}
