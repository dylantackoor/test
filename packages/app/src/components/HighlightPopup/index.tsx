import React, { FunctionComponent } from 'react'

import { T_Comment } from '../../types/react-pdf-highlighter'

export interface HighlightPopupProps {
  comment: T_Comment
}

// TODO: remove scrollbar
export const HighlightPopup: FunctionComponent<HighlightPopupProps> = ({
  comment: { text, emoji },
}) =>
  text ? (
    <div className="Highlight__popup">
      {emoji} {text}
    </div>
  ) : null
