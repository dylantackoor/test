import React, { FunctionComponent, useState } from 'react'
import { useDispatch } from 'react-redux'
import { IconContext, IconType } from 'react-icons'
import { Employee } from '@bigrig/interfaces'
import {
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  Button,
  Label,
  Select,
} from '@windmill/react-ui'

import { useAppSelector } from '../../redux/hooks'
import { assignEquipment } from '../../redux/features/drivers/driverThunk'
import { carrierSelector } from '../../redux/features/carriers/carrierSlice'

export interface AssignModalProps {
  driver: Employee
  Icon: IconType
}

export const AssignModal: FunctionComponent<AssignModalProps> = ({ driver, Icon }) => {
  const dispatch = useDispatch()
  const { carriers } = useAppSelector(carrierSelector)

  // MODAL
  const [isModalOpen, setIsModalOpen] = useState(false)
  const openModal = () => setIsModalOpen(true)
  const closeModal = () => setIsModalOpen(false)

  const carrierIndex = carriers.findIndex(c => c.id === driver.carrier.id)
  const filteredEquipment = carriers[carrierIndex]?.equipment

  const [selectedEquipmentID, setSelectedEquipmentID] = useState<number>(filteredEquipment![0].id)

  const confirmHandler = () => {
    dispatch(
      assignEquipment({
        equipmentID: selectedEquipmentID,
        employeeIDs: [driver.id],
      }),
    )

    closeModal()
  }

  return (
    <div className="flex">
      <IconContext.Provider value={{ color: '#FF2247' }}>
        <Icon className="cursor-pointer" onClick={openModal} />
      </IconContext.Provider>

      <Modal isOpen={isModalOpen} onClose={closeModal}>
        <ModalHeader>Assign Equipment</ModalHeader>
        {filteredEquipment ? (
          <>
            <ModalBody>
              <p>Assign which vehicle to {driver.firstName}?</p>
              <Label>
                <span>Requested Limit</span>

                {/* @ts-ignore */}
                <Select
                  onChange={e => setSelectedEquipmentID(parseInt(e.target.value))}
                  className="mt-1"
                >
                  {filteredEquipment.map(eq => (
                    <option value={eq.id} key={eq.vin}>
                      {eq.type}: {eq.vin}
                    </option>
                  ))}
                </Select>
              </Label>
            </ModalBody>
            <ModalFooter>
              <div>
                <div className="sm:block">
                  <Button layout="outline" onClick={closeModal}>
                    Cancel
                  </Button>
                </div>
                <div className="sm:block">
                  <Button onClick={confirmHandler} type="submit">
                    Accept
                  </Button>
                </div>
              </div>
            </ModalFooter>
          </>
        ) : (
          <>
            <ModalBody>
              <p>No carrier equipment to assign to {driver.firstName}?</p>
            </ModalBody>
            <ModalFooter>
              <div>
                <div className="sm:block">
                  <Button layout="outline" onClick={closeModal}>
                    Cancel
                  </Button>
                </div>
              </div>
            </ModalFooter>
          </>
        )}
      </Modal>
    </div>
  )
}
