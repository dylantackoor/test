/**
 * UNUSED
 *
 * from react-pdf-highlight package demo
 */

import React, { FunctionComponent } from 'react'
import { T_Highlight } from '../types/react-pdf-highlighter'

type T_ManuscriptHighlight = T_Highlight

type Sidebar2Props = {
  highlights: Array<T_ManuscriptHighlight>
  resetHighlights: () => void
  toggleDocument: () => void
}

const updateHash = (highlight: { id: any }) => {
  document.location.hash = `highlight-${highlight.id}`
}

export const Sidebar2: FunctionComponent<Sidebar2Props> = ({
  highlights,
  toggleDocument,
  resetHighlights,
}) => (
  <div className="sidebar" style={{ width: '25vw' }}>
    <div className="description" style={{ padding: '1rem' }}>
      <h2 style={{ marginBottom: '1rem' }}>BigRig Packet Mapper</h2>
      <p>
        <small>To create area highlight hold ⌥ Option key (Alt), then click and drag.</small>
      </p>
    </div>

    <ul className="sidebar__highlights">
      {highlights.map((highlight: any, index: number) => (
        <li
          key={index}
          className="sidebar__highlight"
          onClick={() => {
            updateHash(highlight)
          }}
        >
          <div>
            <strong>{highlight.comment.text}</strong>
            {highlight.content.text ? (
              <blockquote style={{ marginTop: '0.5rem' }}>
                {`${highlight.content.text.slice(0, 90).trim()}…`}
              </blockquote>
            ) : null}
            {highlight.content.image ? (
              <div className="highlight__image" style={{ marginTop: '0.5rem' }}>
                <img src={highlight.content.image} alt={'Screenshot'} />
              </div>
            ) : null}
          </div>
          <div className="highlight__location">Page {highlight.position.pageNumber}</div>
        </li>
      ))}
    </ul>
    <div style={{ padding: '1rem' }}>
      <button onClick={toggleDocument}>Toggle PDF document</button>
    </div>
    {highlights.length > 0 ? (
      <div style={{ padding: '1rem' }}>
        <button onClick={resetHighlights}>Reset highlights</button>
      </div>
    ) : null}
  </div>
)
