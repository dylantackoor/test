import React, { FunctionComponent } from 'react'
import { Transition } from '@windmill/react-ui'
import { Link } from '@reach/router'
import { MdDashboard } from 'react-icons/md'

const routes = {
  name: 'Wiki',
  routes: [
    {
      path: '/drivers',
      name: 'Drivers',
    },
    {
      path: '/carriers',
      name: 'Carriers',
    },
    {
      path: '/brokers',
      name: 'Brokers',
    },
  ],
}

export const SidebarSubmenu: FunctionComponent = () => (
  <li className="relative px-6 py-3" key={routes.name}>
    <button
      className="inline-flex items-center justify-between w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200"
      aria-haspopup="true"
    >
      <span className="inline-flex items-center">
        <MdDashboard className="w-5 h-5" aria-hidden="true" />
        <span className="ml-4">{routes.name}</span>
      </span>
    </button>
    <Transition
      show={true}
      enter="transition-all ease-in-out duration-300"
      enterFrom="opacity-25 max-h-0"
      enterTo="opacity-100 max-h-xl"
      leave="transition-all ease-in-out duration-300"
      leaveFrom="opacity-100 max-h-xl"
      leaveTo="opacity-0 max-h-0"
    >
      <ul
        className="p-2 mt-2 space-y-2 overflow-hidden text-sm font-medium text-gray-500 rounded-md shadow-inner bg-gray-50 dark:text-gray-400 dark:bg-gray-900"
        aria-label="submenu"
      >
        {routes.routes.map(r => (
          <li
            className="px-2 py-1 transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200"
            key={r.name}
          >
            <Link to={r.path} className="w-full">
              {r.name}
            </Link>
          </li>
        ))}
      </ul>
    </Transition>
  </li>
)
