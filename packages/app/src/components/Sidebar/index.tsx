import React, { FunctionComponent } from 'react'
import { DesktopSidebar } from './DesktopSidebar'
import { MobileSidebar } from './MobileSidebar'

export const Sidebar: FunctionComponent = () => (
  <>
    <DesktopSidebar />
    <MobileSidebar />
  </>
)
