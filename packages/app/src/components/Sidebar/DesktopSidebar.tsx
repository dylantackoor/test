import React, { FunctionComponent } from 'react'

import { SidebarContent } from './SidebarContent'

export const DesktopSidebar: FunctionComponent = () => (
  <aside className="z-30 flex-shrink-0 hidden w-64 overflow-y-auto bg-white dark:bg-gray-800 lg:block">
    <SidebarContent />
  </aside>
)
