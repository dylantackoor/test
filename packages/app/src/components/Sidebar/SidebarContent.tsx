import React, { FunctionComponent } from 'react'
import { Link } from '@reach/router'
import { Button } from '@windmill/react-ui'
import { MdEmail } from 'react-icons/md'
// import { GrDocumentPdf } from 'react-icons/gr'
import { SidebarSubmenu } from './SidebarSubMenu'

const routes = [
  {
    path: '/setups',
    icon: <MdEmail />,
    name: 'Inbox',
  },
  // {
  //   path: '/packets',
  //   icon: <GrDocumentPdf />,
  //   name: 'Packet Mapping',
  // },
]

export const SidebarContent: FunctionComponent = () => (
  <div className="py-4 text-gray-500 dark:text-gray-400">
    <a className="ml-6 text-lg font-bold text-gray-800 dark:text-gray-200">BigRig</a>
    <ul className="mt-6">
      {routes.map(route => (
        <li className="relative px-6 py-3" key={route.name}>
          <Link
            to={route.path}
            className="inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200"
          >
            {route.icon}
            <span className="ml-4">{route.name}</span>
          </Link>
        </li>
      ))}
      <SidebarSubmenu />
    </ul>

    <div className="px-6 my-6">
      <Button
        style={{ backgroundColor: '#FF2247' }}
        onClick={() =>
          window.open('https://admin.google.com/ac/users?action_id=ADD_USER', '_blank')
        }
      >
        Create account
        <span className="ml-2" aria-hidden="true">
          +
        </span>
      </Button>
      <Button
        style={{ marginTop: '1em', backgroundColor: '#FF2247' }}
        onClick={() =>
          window.open('https://us-central1-bigrig-304700.cloudfunctions.net/auth_init', '_blank')
        }
      >
        Add Gmail integration
      </Button>
    </div>
  </div>
)
