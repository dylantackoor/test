import React, { FunctionComponent } from 'react'
import { Button, Card, CardBody } from '@windmill/react-ui'
import { AiFillDelete } from 'react-icons/ai'

import { T_Highlight } from '../types/react-pdf-highlighter'

export interface MyFormProps {
  mappedInputs: Array<T_Highlight>
  onSubmit: () => void
}

export const MyForm: FunctionComponent<MyFormProps> = props => (
  <div>
    <p style={{ textAlign: 'center' }}>Mapped Inputs</p>

    {props.mappedInputs.length <= 0 ? (
      <p style={{ textAlign: 'center' }}>Map something!</p>
    ) : (
      <>
        {props.mappedInputs.map(i => (
          <Card
            key={i.id}
            className="flex h-48"
            style={{ margin: '15px', backgroundColor: 'rgb(255, 34, 71)' }}
          >
            <img className="object-cover w-1/3" src={i.content.image} alt={i.comment.text} />

            <CardBody className="text-white">
              <p>Title: {i.comment.text}</p>
              <p>Page: {i.position.pageNumber}</p>
              <p>
                Position:{' '}
                {JSON.stringify(
                  {
                    x1: i.position.boundingRect.x1,
                    x2: i.position.boundingRect.x2,
                    y1: i.position.boundingRect.y1,
                    y2: i.position.boundingRect.y2,
                  },
                  null,
                  2,
                )}
              </p>
              {i.comment.emoji && <p>Type: {i.comment.emoji}</p>}
              <AiFillDelete className="cursor-pointer" onClick={() => alert('TODO')} />
            </CardBody>
          </Card>
        ))}
        <Button
          style={{ marginLeft: '15px', backgroundColor: 'rgb(255, 34, 71)' }}
          onClick={props.onSubmit}
        >
          Submit
        </Button>
      </>
    )}
  </div>
)
