import React, { FunctionComponent, useState } from 'react'
import Form from '@rjsf/bootstrap-4'
import { Modal, ModalHeader, ModalBody, Button, TableRow } from '@windmill/react-ui'
import { Registration } from '@bigrig/interfaces'

export const TableRowModal: FunctionComponent<{ registration: Registration }> = ({
  children,
  registration,
}) => {
  // MODAL
  const [isModalOpen, setIsModalOpen] = useState(false)
  const openModal = () => setIsModalOpen(true)
  const closeModal = () => setIsModalOpen(false)

  // FORM
  const [formData, setFormData] = useState(registration.carrier)
  const handleSubmit = async (formData: any) => {
    alert(`TODO: waiting on email forwarding implementation: ${JSON.stringify(formData)}`)
    // await confirm({
    //   packetID: registration.packet.id,
    //   answers: formData,
    //   registrationID: registration.id,
    // })
    closeModal()
  }

  const getPdfURL = (brokerName: string): string => {
    if (brokerName === 'Broker #1') {
      return '/pdfs/express.pdf'
    } else {
      return '/pdfs/sample.pdf'
    }
  }

  const pdfURL = getPdfURL(registration.broker.name)

  return (
    <>
      <TableRow onClick={openModal}>{children}</TableRow>

      <Modal isOpen={isModalOpen} onClose={closeModal}>
        <ModalHeader>Form Confirmation</ModalHeader>
        <ModalBody>
          <div className="flex" style={{ height: '85vh' }}>
            <div className="w-1/2">
              <p className="text-xl">Preview</p>
              <object
                style={{ width: '100%', height: '100%' }}
                data={pdfURL}
                type="application/pdf"
              >
                <iframe
                  title="pdfPreview"
                  src={`https://docs.google.com/viewer?url=${pdfURL}&embedded=true`}
                ></iframe>
              </object>
            </div>
            <Form
              autoComplete="off"
              className="w-1/2 pl-4 overflow-y-scroll"
              // schema={registration.packet!.schema}
              schema={{}}
              formData={formData}
              onChange={e => setFormData(e.formData)}
              onSubmit={e => handleSubmit(e.formData)}
            >
              <div>
                <div className="sm:block">
                  <Button layout="outline" onClick={closeModal}>
                    Cancel
                  </Button>
                </div>
                <div className="sm:block">
                  <Button type="submit">Accept</Button>
                </div>
              </div>
            </Form>
          </div>
        </ModalBody>
      </Modal>
    </>
  )
}
