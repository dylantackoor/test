import React, { Component, ChangeEventHandler } from 'react'
import { PdfLoader, PdfHighlighter, Highlight, Popup, AreaHighlight } from 'react-pdf-highlighter'

import { Spinner, Tip, HighlightPopup } from '..'
import './Annotator.css'
import { T_Highlight, T_NewHighlight, T_PDFJS_Document } from '../../types/react-pdf-highlighter'

// setPdfWorker(PDFWorker)

const getNextId = () => String(Math.random()).slice(2)

const parseIdFromHash = () => document.location.hash.slice('#highlight-'.length)

const resetHash = () => {
  document.location.hash = ''
}

type AnnotatorProps = {
  addMappedInput: (input: T_Highlight) => void
  pdfFile: File | null
  setPdfFile: any
}

type AnnotatorState = {
  url: string
  highlights: Array<T_Highlight>
  pdfURL: string
}

export class Annotator extends Component<AnnotatorProps, AnnotatorState> {
  state: AnnotatorState = {
    url: '',
    highlights: [],
    pdfURL: '',
  }

  resetHighlights = () =>
    this.setState({
      highlights: [],
    })

  scrollViewerTo = (highlight: any) => {}

  scrollToHighlightFromHash = () => {
    const highlight = this.getHighlightById(parseIdFromHash())

    if (highlight) {
      this.scrollViewerTo(highlight)
    }
  }

  componentDidMount = () =>
    window.addEventListener('hashchange', this.scrollToHighlightFromHash, false)

  getHighlightById = (id: string) => this.state.highlights.find(highlight => highlight.id === id)

  addHighlight(highlight: T_NewHighlight) {
    const newHighlight = { ...highlight, id: getNextId() }

    this.props.addMappedInput(newHighlight)
    this.setState({
      highlights: [newHighlight, ...this.state.highlights],
    })
  }

  // TODO: make this update form
  updateHighlight(highlightId: string, position: Object, content: Object) {
    const highlights = this.state.highlights.map(h => {
      const { id, position: originalPosition, content: originalContent, ...rest } = h
      return id === highlightId
        ? {
            id,
            position: { ...originalPosition, ...position },
            content: { ...originalContent, ...content },
            ...rest,
          }
        : h
    })

    this.setState({ highlights })
  }

  handlePdfSelect: ChangeEventHandler<HTMLInputElement> = e => {
    const attachment = URL.createObjectURL(e.target.files![0])
    this.setState({ pdfURL: attachment })

    this.props.setPdfFile(e.target.files![0])

    this.setState({
      url: attachment,
      highlights: [],
    })
  }

  render() {
    const { url, highlights } = this.state

    return (
      <>
        <p style={{ textAlign: 'center' }}>TODO: add better title + tutorial</p>
        <label htmlFor="myfile">Select a file:</label>
        <input
          onChange={this.handlePdfSelect}
          type="file"
          accept="application/pdf"
          id="myfile"
          name="myfile"
        ></input>
        {this.state.url !== '' && (
          <PdfLoader url={url} beforeLoad={<Spinner />}>
            {(pdfDocument: T_PDFJS_Document) => (
              <PdfHighlighter
                pdfDocument={pdfDocument}
                highlights={highlights}
                enableAreaSelection={(event: KeyboardEvent) => event.altKey}
                onScrollChange={resetHash}
                // pdfScaleValue="page-width"
                scrollRef={(scrollTo: any) => {
                  this.scrollViewerTo = scrollTo
                  this.scrollToHighlightFromHash()
                }}
                onSelectionFinished={(
                  position: any,
                  content: any,
                  hideTipAndSelection: any,
                  transformSelection: any,
                ) => (
                  <Tip
                    onOpen={transformSelection}
                    onConfirm={comment => {
                      this.addHighlight({ content, position, comment })
                      hideTipAndSelection()
                    }}
                  />
                )}
                highlightTransform={(
                  highlight: any,
                  index: any,
                  setTip: any,
                  hideTip: any,
                  viewportToScaled: any,
                  screenshot: any,
                  isScrolledTo: any,
                ) => {
                  const isTextHighlight = !(highlight.content && highlight.content.image)

                  const component = isTextHighlight ? (
                    <Highlight
                      isScrolledTo={isScrolledTo}
                      position={highlight.position}
                      comment={highlight.comment}
                    />
                  ) : (
                    <AreaHighlight
                      highlight={highlight}
                      onChange={(boundingRect: any) => {
                        this.updateHighlight(
                          highlight.id,
                          { boundingRect: viewportToScaled(boundingRect) },
                          { image: screenshot(boundingRect) },
                        )
                      }}
                    />
                  )

                  return (
                    <Popup
                      key={index}
                      children={component}
                      popupContent={<HighlightPopup {...highlight} />}
                      onMouseOut={hideTip}
                      onMouseOver={(popupContent: any) =>
                        setTip(highlight, (highlight: any) => popupContent)
                      }
                    />
                  )
                }}
              />
            )}
          </PdfLoader>
        )}
      </>
    )
  }
}
