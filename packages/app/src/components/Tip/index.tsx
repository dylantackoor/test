import React, {
  useState,
  useEffect,
  FunctionComponent,
  ChangeEventHandler,
  FormEventHandler,
  MouseEventHandler,
} from 'react'

import './Tip.css'

export type TipProps = {
  onConfirm: (comment: { text: string; emoji: string }) => void
  onOpen: () => void
  onUpdate?: () => void
}

export const Tip: FunctionComponent<TipProps> = ({ onConfirm, onOpen, onUpdate }) => {
  const [compact, setCompact] = useState(true)
  const [text, setText] = useState('AvatarURL')
  const [emoji, setEmoji] = useState('')
  const columns = [
    'Date (Day)',
    'Date (Full)',
    'Date (Month)',
    'Date (Year)',

    'name',
    'avatarURL',
    'ownerName',
    'businessAddress',
    'authStartDate',
    'email',
    'email2',
    'companyPhone',
    'companyPhone2',
    'w9PersonalName',
    'w9EntityName',
    'w9EntityType',
    'w9Address',
    'refrigerationBreakdownDeductable',
    'cargoDeductable',
    'factoringCompanyName',
    'factoringCompanyAddress',
    'factoringCompanyPhone',
    'factoringCompanyEmail',
    'bankName',
    'bankAddress',
    'bankPhone',
    'routingNumber',
    'checkingNumber',
    'MC',
    'EIN',
    'DOT',
    'TIN',
    'insuranceAgentName',
    'insurancePhone',
    'insuranceFax',
    'insuranceEmail',
    'employees',
    'equipment',
    'autoLiabilityAmount',
    'autoLiabilityCoverageInsurerName',
    'autoLiabilityExpireDate',
    'autoLiabilityPolicyNumber',
    'cargoLiabilityAmount',
    'cargoLiabilityCoverageInsurerName',
    'cargoLiabilityExpireDate',
    'cargoLiabilityPolicyNumber',
    'generalLiabilityAmount',
    'generalLiabilityCoverageInsurerName',
    'generalLiabilityExpireDate',
    'generalLiabilityPolicyNumber',
    'hazmatCoverageAmount',
    'hazmatCoverageInsurerName',
    'hazmatLiabilityExpireDate',
    'hazmatLiabilityPolicyNumber',
    'physicalDamageCoverageAmount',
    'physicalDamageExpireDate',
    'physicalDamageInsurerName',
    'physicalDamagePolicyNumber',
    'trailerInterchageCoverageAmount',
    'trailerInterchageExpireDate',
    'trailerInterchagePolicyNumber',
    'trailerInterchargeCoverageInsurerName',
  ]

  useEffect(() => onUpdate?.(), [compact])

  const onChange: ChangeEventHandler<HTMLSelectElement> = e => setText(e.target.value)
  const onClick: MouseEventHandler<HTMLDivElement> = () => {
    onOpen()
    setCompact(false)
  }
  const onSubmit: FormEventHandler<HTMLFormElement> = e => {
    e.preventDefault()
    onConfirm({ text, emoji })
  }

  return (
    <div className="Tip">
      {compact ? (
        <div className="Tip__compact" onClick={onClick}>
          Map Form Field
        </div>
      ) : (
        <form className="Tip__card" onSubmit={onSubmit}>
          <div>
            <select name="column" onChange={onChange}>
              {columns.map(column => (
                <option key={column} value={column}>
                  {column}
                </option>
              ))}
            </select>
          </div>
          <div>
            <input type="submit" value="Save" />
          </div>
        </form>
      )}
    </div>
  )
}
