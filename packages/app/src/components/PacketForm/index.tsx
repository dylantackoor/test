import React, { useState, FunctionComponent } from 'react'
import { UiSchema } from '@rjsf/core'
import Form from '@rjsf/bootstrap-4'
import { JSONSchema7 } from 'json-schema'

export interface PacketSchema {
  schema: JSONSchema7
  data: unknown
  pdf?: Blob
  uiSchema?: UiSchema
  coordinates?: {
    title: string
    page: number
    x: number
    y: number
  }[]
}

const samplePacket: PacketSchema = {
  schema: {
    title: 'My title',
    description: 'My description',
    type: 'object',
    properties: {
      firstName: {
        type: 'string',
      },
      Street_Address: { type: 'string' },
      City: { type: 'string' },
      State: { type: 'string' },
      done: {
        type: 'boolean',
      },
    },
  },
  data: {
    firstName: 'Dylan',
    Street_Address: '8520 sw 149th ave',
    City: 'Miami',
    State: 'FL',
    done: false,
  },
}

export const PacketForm: FunctionComponent = () => {
  const [formData, setFormData] = useState(samplePacket.data)

  const handleSubmit = (formData: any) => {
    alert(JSON.stringify(formData))
  }

  return (
    <>
      {/* <link
        rel="stylesheet"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
        crossOrigin="anonymous"
      ></link> */}
      <Form
        schema={samplePacket.schema}
        uiSchema={samplePacket.uiSchema}
        formData={formData}
        onChange={e => setFormData(e.formData)}
        onSubmit={e => handleSubmit(e.formData)}
      />
    </>
  )
}
