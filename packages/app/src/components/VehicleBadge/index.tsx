import React, { FunctionComponent } from 'react'
import { Badge } from '@windmill/react-ui'

export const VehicleBadge: FunctionComponent<{
  type?: 'reefer' | 'van' | 'powerUnit' | 'flatbed'
}> = ({ type }) => {
  if (type === 'flatbed') {
    return <Badge type="success">Flatbed</Badge>
  } else if (type === 'van') {
    return <Badge type="primary">Van</Badge>
  } else if (type === 'powerUnit') {
    return <Badge type="warning">Power Unit</Badge>
  } else if (type === 'reefer') {
    return <Badge type="warning">Reefer</Badge>
  } else {
    return <Badge type="neutral">None</Badge>
  }
}
