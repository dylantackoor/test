import React, { FunctionComponent, useState } from 'react'
import { Modal, ModalBody, Button } from '@windmill/react-ui'
import { IconContext } from 'react-icons'
import { RiAddCircleFill } from 'react-icons/ri'
import Form from '@rjsf/bootstrap-4'
import { JSONSchema7 } from 'json-schema'

export interface FormModalProps {
  schema: JSONSchema7
}

export const FormModal: FunctionComponent<FormModalProps> = ({ schema }) => {
  // MODAL
  const [isModalOpen, setIsModalOpen] = useState(false)
  const openModal = () => setIsModalOpen(true)
  const closeModal = () => setIsModalOpen(false)

  // FORM
  const [formData, setFormData] = useState({})

  const formID = 'formID'
  return (
    <div className="flex">
      <IconContext.Provider value={{ color: '#FF2247', size: '1.5em' }}>
        <RiAddCircleFill className="cursor-pointer m-6" onClick={openModal} />
      </IconContext.Provider>
      <style>{`
        #${formID} {
          overflow-y: scroll !important;
        }
      `}</style>
      <Modal
        id={formID}
        style={{ maxHeight: '95vh', overflowY: 'scroll' }}
        isOpen={isModalOpen}
        onClose={closeModal}
      >
        <ModalBody>
          <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
            integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
            crossOrigin="anonymous"
          />
          <Form
            autoComplete="off"
            formData={formData}
            onChange={e => setFormData(e.formData)}
            onSubmit={e => alert(`Auth error sending body: ${JSON.stringify(e.formData)}`)}
            schema={schema}
          >
            <div>
              <div className="sm:block">
                <Button layout="outline" onClick={closeModal}>
                  Cancel
                </Button>
              </div>
              <div className="sm:block">
                <Button type="submit">Accept</Button>
              </div>
            </div>
          </Form>
        </ModalBody>
      </Modal>
    </div>
  )
}
