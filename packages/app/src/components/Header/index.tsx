import React, { FunctionComponent, useContext, useState } from 'react'
import { Input, Dropdown, DropdownItem, WindmillContext } from '@windmill/react-ui'
import { IconContext } from 'react-icons'
import {
  MdMenu,
  MdSettings,
  MdSearch,
  MdAccountCircle,
  MdFlashOn,
  MdFlashOff,
  MdExitToApp,
  MdAirportShuttle,
} from 'react-icons/md'

// TODO: replace with rtk
import { useSidebarContext } from '../../hooks/SideBar'
import { useSearchContext } from '../../hooks/useSearchContext'
import { useAppDispatch } from '../../redux/store'
import { logout } from '../../redux/features/auth/authThunks'

const RedIcon: FunctionComponent = ({ children }) => (
  <IconContext.Provider value={{ color: '#FF2247' }}>{children}</IconContext.Provider>
)

export const Header: FunctionComponent = () => {
  // TODO: replace with redux
  const { searchTarget, setSearchTarget } = useSearchContext()
  const { toggleSidebar } = useSidebarContext()
  const { mode, toggleMode } = useContext(WindmillContext)
  const dispatch = useAppDispatch()

  const [isNotificationsMenuOpen, setIsNotificationsMenuOpen] = useState(false)
  const [isLabelsMenuOpen, setIsLabelsMenuOpen] = useState(false)
  const [isProfileMenuOpen, setIsProfileMenuOpen] = useState(false)

  const handleLabelsClick = () => setIsLabelsMenuOpen(!isLabelsMenuOpen)
  const handleNotificationsClick = () => setIsNotificationsMenuOpen(!isNotificationsMenuOpen)
  const handleProfileClick = () => setIsProfileMenuOpen(!isProfileMenuOpen)

  const handleLogout = () => {
    console.log('handle logout')
    dispatch(logout())
  }

  return (
    <header className="z-40 py-4 bg-white shadow-bottom dark:bg-gray-800">
      <div className="container flex items-center justify-between h-full px-6 mx-auto text-red-600 dark:text-red-300">
        {/* <!-- Mobile hamburger --> */}
        <button
          className="p-1 mr-5 -ml-1 rounded-md lg:hidden focus:outline-none focus:shadow-outline-purple"
          onClick={toggleSidebar}
          aria-label="Menu"
        >
          <MdMenu className="w-6 h-6" aria-hidden="true" />
        </button>

        {/* <!-- Search input --> */}
        <div className="flex justify-center flex-1 lg:mr-32">
          <div className="relative w-full max-w-xl mr-6 focus-within:text-red-500">
            <div className="absolute inset-y-0 flex items-center pl-2">
              <MdSearch className="w-4 h-4" aria-hidden="true" />
            </div>
            <Input
              value={searchTarget}
              onChange={e => setSearchTarget(e.target.value)}
              className="pl-8 text-gray-700"
              placeholder="Search"
              css={null}
              aria-label="Search"
            />
          </div>
        </div>

        <ul className="flex items-center flex-shrink-0 space-x-6">
          {/* <!-- Theme toggler --> */}
          <li className="flex">
            <button
              className="rounded-md focus:outline-none focus:shadow-outline-purple"
              onClick={toggleMode}
              aria-label="Toggle color mode"
            >
              {mode === 'dark' ? (
                <RedIcon>
                  <MdFlashOff className="w-5 h-5" aria-hidden="true" />
                </RedIcon>
              ) : (
                <RedIcon>
                  <MdFlashOn className="w-5 h-5" aria-hidden="true" />
                </RedIcon>
              )}
            </button>
          </li>

          {/* <!-- Notifications menu --> */}
          <li className="relative">
            <button
              className="relative align-middle rounded-md focus:outline-none focus:shadow-outline-purple"
              onClick={handleNotificationsClick}
              aria-label="Notifications"
              aria-haspopup="true"
            >
              <RedIcon>
                <MdAirportShuttle className="w-5 h-5" aria-hidden="true" />
              </RedIcon>
            </button>

            <Dropdown
              align="right"
              isOpen={isNotificationsMenuOpen}
              onClose={() => setIsNotificationsMenuOpen(false)}
            >
              {[].map((carrier: any) => (
                <DropdownItem key={carrier.id} onClick={() => alert('TODO: filter to Carrier #1')}>
                  <span>{carrier.name}</span>
                </DropdownItem>
              ))}
            </Dropdown>
          </li>

          {/* <!-- Profile menu --> */}
          <li className="relative">
            <button
              className="relative align-middle rounded-md focus:outline-none focus:shadow-outline-purple"
              onClick={handleProfileClick}
              aria-label="Account"
              aria-haspopup="true"
            >
              <RedIcon>
                <MdAccountCircle className="w-5 h-5" aria-hidden="true" />
              </RedIcon>{' '}
            </button>
            <Dropdown
              align="right"
              isOpen={isProfileMenuOpen}
              onClose={() => setIsProfileMenuOpen(false)}
            >
              <DropdownItem
                tag="a"
                href="#"
                onClick={() =>
                  alert("There isn't a settings page, just thought this would be useful later")
                }
              >
                <MdSettings className="w-4 h-4 mr-3" aria-hidden="true" />
                <span>Settings</span>
              </DropdownItem>
              <DropdownItem>
                <MdExitToApp className="w-4 h-4 mr-3" aria-hidden="true" />
                <a onClick={handleLogout}>Log out</a>
              </DropdownItem>
            </Dropdown>
          </li>
        </ul>
      </div>
    </header>
  )
}
