import React, { useEffect, useState, FunctionComponent } from 'react'
import { RouteComponentProps } from '@reach/router'
import { Carrier } from '@bigrig/interfaces'
import { JSONSchema7 } from 'json-schema'
import {
  Table,
  TableHeader,
  TableCell,
  TableRow,
  TableBody,
  TableFooter,
  TableContainer,
  Avatar,
  Pagination,
} from '@windmill/react-ui'

import { AppLayout } from '../layouts'
import { FormModal } from '../components'
import { useSearchContext } from '../hooks/useSearchContext'
import { useAppSelector } from '../redux/hooks'
import { carrierSelector } from '../redux/features/carriers/carrierSlice'

export const CarriersRoute: FunctionComponent<RouteComponentProps> = () => {
  const { carriers, loading } = useAppSelector(carrierSelector)

  // TODO: get vehicle type sums

  const { searchTarget } = useSearchContext()
  const [pageTable, setPageTable] = useState(1)
  const [dataTable, setDataTable] = useState<Carrier[]>(carriers)

  const addCarrierSchema: JSONSchema7 = {
    title: 'New Carrier Form',
    type: 'object',
    properties: {
      name: {
        title: 'Name',
        type: 'string',
      },
      email: {
        title: 'Email',
        type: 'string',
      },
      logoURL: {
        title: 'Logo URL',
        type: 'string',
      },

      reefers: { title: 'Reefers', type: 'integer' },
      vans: { title: 'Vans', type: 'integer' },
      flatbeds: { title: 'Flat Beds', type: 'integer' },
      powerUnits: { title: 'Power Units', type: 'integer' },

      drivers: {
        type: 'array',
        title: 'Add new drivers?',
        items: {
          type: 'object',
          title: 'New Driver',
          properties: {
            firstName: { title: 'First name', type: 'string' },
            lastName: { title: 'Last name', type: 'string' },
            email: { title: 'Email', type: 'string' },
            phone: { title: 'Phone', type: 'integer' },
            fax: { title: 'Fax', type: 'integer' },
            DOT: { title: 'DOT', type: 'integer' },
            MC: { title: 'MC', type: 'integer' },
            EIN: { title: 'EIN', type: 'integer' },
            SCAC: { title: 'SCAC', type: 'integer' },
          },
        },
      },
    },
  }

  const resultsPerPage = 10
  const totalResults = carriers.length

  const onPageChangeTable = (p: number) => setPageTable(p)

  useEffect(() => {
    const filteredData = carriers.filter(
      carrier => carrier.email.includes(searchTarget) || carrier.name.includes(searchTarget),
    )
    setDataTable(filteredData.slice((pageTable - 1) * resultsPerPage, pageTable * resultsPerPage))
  }, [searchTarget, carriers, pageTable])

  return (
    <AppLayout>
      <FormModal schema={addCarrierSchema} />
      <TableContainer className="mb-8">
        <Table>
          <TableHeader>
            <tr>
              <TableCell>ID</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Email</TableCell>
              {/* <TableCell>flatbeds</TableCell>
              <TableCell>powerUnits</TableCell>
              <TableCell>reefers</TableCell> */}
            </tr>
          </TableHeader>
          <TableBody>
            {dataTable.map(row => (
              <TableRow key={row.id}>
                <TableCell>
                  <p>{row.id}</p>
                </TableCell>
                <TableCell>
                  <div className="flex items-center text-sm">
                    <Avatar
                      className="hidden mr-3 md:block"
                      src={`${row.avatarURL}`}
                      alt="User avatar"
                    />
                    <div>
                      <p className="font-semibold">{row.name}</p>
                      <p className="text-xs text-gray-600 dark:text-gray-400">{row.email}</p>
                    </div>
                  </div>
                </TableCell>
                <TableCell>
                  <p>{row.email}</p>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <TableFooter>
          <Pagination
            totalResults={totalResults}
            resultsPerPage={resultsPerPage}
            onChange={onPageChangeTable}
            label="Table navigation"
          />
        </TableFooter>
      </TableContainer>
    </AppLayout>
  )
}
