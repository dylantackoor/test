import React, { FunctionComponent, useEffect, useState } from 'react'
import { Link, useNavigate, RouteComponentProps } from '@reach/router'
import { Label, Input, Button } from '@windmill/react-ui'
import { AuthLoginRequest } from '@bigrig/interfaces'

import { useAppDispatch } from '../redux/store'
import { login } from '../redux/features/auth/authThunks'
import { useAppSelector } from '../redux/hooks'
import { userSelector } from '../redux/features/user/userSlice'

const LogoImg = 'https://www.bigrigdispatchers.com/wp-content/uploads/2018/04/BigRig_White-Red.png'

export const LoginRoute: FunctionComponent<RouteComponentProps> = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const user = useAppSelector(userSelector)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const handleSignIn = async () => {
    const loginOpts: AuthLoginRequest = {
      email,
      password,
    }

    if (!email || email === '') {
      return alert('Fill out email')
    } else if (!password || password === '') {
      return alert('Fill out password')
    }

    dispatch(login(loginOpts))
  }

  useEffect(() => {
    if (user.id !== '') {
      navigate('/brokers')
    }
  }, [user])

  return (
    <div className="flex items-center min-h-screen p-6 bg-gray-50 dark:bg-gray-900">
      <div className="flex-1 h-full max-w-4xl mx-auto overflow-hidden bg-white rounded-lg shadow-xl dark:bg-gray-800">
        <div className="flex flex-col overflow-y-auto md:flex-row">
          <div className="h-32 md:h-auto md:w-1/2">
            <img
              aria-hidden="true"
              className="object-cover w-full h-full dark:hidden"
              src={LogoImg}
              alt="Logo"
            />
            <img
              aria-hidden="true"
              className="hidden object-cover w-full h-full dark:block"
              src={LogoImg}
              alt="Logo"
            />
          </div>
          <main className="flex items-center justify-center p-6 sm:p-12 md:w-1/2">
            <div className="w-full">
              <h1 className="mb-4 text-xl font-semibold text-gray-700 dark:text-gray-200">Login</h1>
              <Label>
                <span>Email</span>
                {/* @ts-ignore */}
                <Input
                  className="mt-1"
                  value={email}
                  onChange={e => setEmail(e.target.value)}
                  type="email"
                  placeholder="example@bigrig.dev"
                />
              </Label>

              <Label className="mt-4">
                <span>Password</span>
                {/* @ts-ignore */}
                <Input
                  className="mt-1"
                  value={password}
                  onChange={e => setPassword(e.target.value)}
                  type="password"
                  placeholder="***************"
                />
              </Label>

              <Button
                onClick={handleSignIn}
                className="mt-4"
                block
                style={{ backgroundColor: '#FF2247' }}
              >
                Log in
              </Button>

              <hr className="my-8" />

              {/* <Button block layout="outline">
                <AiOutlineGoogle className="w-4 h-4 mr-2" aria-hidden="true" />
                Google
              </Button> */}

              {/* <p className="mt-4">
                <Link href="/forgot-password">
                  <a className="text-sm font-medium text-red-600 dark:text-red-400 hover:underline">
                    Forgot your password?
                  </a>
                </Link>
              </p> */}
              <p className="mt-1">
                <Link
                  to="/register"
                  className="text-sm font-medium hover:underline"
                  style={{ color: '#FF2247' }}
                >
                  Create account
                </Link>
              </p>
            </div>
          </main>
        </div>
      </div>
    </div>
  )
}
