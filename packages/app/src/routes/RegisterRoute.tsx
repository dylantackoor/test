import React, { FunctionComponent, useEffect, useState } from 'react'
import { Link, useNavigate, RouteComponentProps } from '@reach/router'
import { Input, Label, Button } from '@windmill/react-ui'
import { AuthCreateRequest } from '@bigrig/interfaces'

import { useAppDispatch } from '../redux/store'
import { register } from '../redux/features/auth/authThunks'
import { useAppSelector } from '../redux/hooks'
import { userSelector } from '../redux/features/user/userSlice'

const logoImg = 'https://www.bigrigdispatchers.com/wp-content/uploads/2018/04/BigRig_White-Red.png'

export const RegisterRoute: FunctionComponent<RouteComponentProps> = () => {
  const navigate = useNavigate()
  const dispatch = useAppDispatch()
  const user = useAppSelector(userSelector)

  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [confirmation, setConfirmation] = useState('')

  const handleSignUp = async () => {
    const signUpOpts: AuthCreateRequest = {
      email,
      password,
      confirmation,
    }

    if (!email || email === '') {
      return alert('Fill out email')
    } else if (!password || password === '') {
      return alert('Fill out password')
    } else if (!confirmation || confirmation === '') {
      return alert('Fill out password confirmation')
    }

    dispatch(register(signUpOpts))
  }

  useEffect(() => {
    if (user.id) {
      navigate('/setups')
    }
  }, [user])

  return (
    <div className="flex items-center min-h-screen p-6 bg-gray-50 dark:bg-gray-900">
      <div className="flex-1 h-full max-w-4xl mx-auto overflow-hidden bg-white rounded-lg shadow-xl dark:bg-gray-800">
        <div className="flex flex-col overflow-y-auto md:flex-row">
          <div className="h-32 md:h-auto md:w-1/2">
            <img
              aria-hidden="true"
              className="object-cover w-full h-full dark:hidden"
              src={logoImg}
              alt="Logo"
            />
            <img
              aria-hidden="true"
              className="hidden object-cover w-full h-full dark:block"
              src={logoImg}
              alt="Logo"
            />
          </div>
          <main className="flex items-center justify-center p-6 sm:p-12 md:w-1/2">
            <div className="w-full">
              <h1 className="mb-4 text-xl font-semibold text-gray-700 dark:text-gray-200">
                Create account
              </h1>
              <Label>
                <span>Email</span>
                {/* @ts-ignore */}
                <Input
                  value={email}
                  onChange={e => setEmail(e.target.value)}
                  className="mt-1"
                  type="email"
                  placeholder="john@doe.com"
                />
              </Label>
              <Label className="mt-4">
                <span>Password</span>
                {/* @ts-ignore */}
                <Input
                  value={password}
                  onChange={e => setPassword(e.target.value)}
                  className="mt-1"
                  placeholder="***************"
                  type="password"
                />
              </Label>
              <Label className="mt-4">
                <span>Confirm password</span>
                {/* @ts-ignore */}
                <Input
                  value={confirmation}
                  onChange={e => setConfirmation(e.target.value)}
                  className="mt-1"
                  placeholder="***************"
                  type="password"
                />
              </Label>

              <Button onClick={handleSignUp} className="mt-4" block>
                Create account
              </Button>

              <hr className="my-8" />

              {/* <Button block layout="outline">
                <AiOutlineGoogle className="w-4 h-4 mr-2" aria-hidden="true" />
                Google
              </Button> */}

              <p className="mt-1">
                <Link
                  to="/login"
                  className="text-sm font-medium text-red-600 dark:text-red-400 hover:underline"
                >
                  Already have an account? Login
                </Link>
              </p>
            </div>
          </main>
        </div>
      </div>
    </div>
  )
}
