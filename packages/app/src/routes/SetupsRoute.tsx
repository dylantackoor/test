import React, { useState, useEffect, FunctionComponent } from 'react'
import { RouteComponentProps } from '@reach/router'
import { JSONResponse, Registration } from '@bigrig/interfaces'
import {
  Table,
  TableHeader,
  TableCell,
  TableBody,
  TableFooter,
  TableContainer,
  Badge,
  Avatar,
  Pagination,
  TableRow,
} from '@windmill/react-ui'

// import { TableRowModal } from '../components'
import { AppLayout } from '../layouts'
import { callApi } from '../redux/api'
// import { useAppSelector } from '../redux/hooks'
// import { setupSelector } from '../redux/features/setups/setupsSlice'

export const SetupsRoute: FunctionComponent<RouteComponentProps> = () => {
  // TODO: use this instead of crazy useEffect
  // const setups = useAppSelector(setupSelector)

  const [registrations, setRegistrations] = useState<Registration[]>([])
  const [pageTable, setPageTable] = useState(1)
  const [dataTable, setDataTable] = useState<Registration[]>(registrations)

  const resultsPerPage = 10
  const totalResults = registrations.length

  const onPageChangeTable = (p: number) => setPageTable(p)

  useEffect(() => {
    const getData = async () => {
      const pls = await callApi<JSONResponse<Registration[]>>('/registrations/find', {
        body: '{}',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        method: 'POST',
      })

      setRegistrations(pls.data!)
      setDataTable(pls.data!)
    }

    getData()
    // setDataTable(registrations.slice((pageTable - 1) * resultsPerPage, pageTable * resultsPerPage))
  }, [])

  return (
    <AppLayout>
      <TableContainer className="mb-8">
        <Table>
          <TableHeader>
            <tr>
              <TableCell>From</TableCell>
              <TableCell>To</TableCell>
              <TableCell>Carrier</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>Date</TableCell>
            </tr>
          </TableHeader>
          <TableBody>
            {dataTable.map(registration => (
              <TableRow key={registration.id}>
                <TableCell>
                  <div className="flex items-center text-sm">
                    <Avatar
                      className="hidden mr-3 md:block"
                      src={`${registration.broker.avatarURL}`}
                      alt="User avatar"
                    />
                    <div>
                      <p className="font-semibold">{registration.broker.name}</p>
                      <p className="text-xs text-gray-600 dark:text-gray-400">
                        {registration.broker.email}
                      </p>
                    </div>
                  </div>
                </TableCell>
                <TableCell>
                  <div className="flex items-center text-sm">
                    <div>
                      <p className="font-semibold">{registration.employee.firstName}</p>
                      <p className="text-xs text-gray-600 dark:text-gray-400">
                        {registration.employee.email}
                      </p>
                    </div>
                  </div>
                </TableCell>
                <TableCell>
                  <div className="flex items-center text-sm">
                    <Avatar
                      className="hidden mr-3 md:block"
                      src={`${registration.carrier.avatarURL}`}
                      alt="User avatar"
                    />
                    <div>
                      <p className="font-semibold">{registration.carrier.name}</p>
                      <p className="text-xs text-gray-600 dark:text-gray-400">
                        {registration.carrier.email}
                      </p>
                    </div>
                  </div>
                </TableCell>
                <TableCell>
                  <Badge type={registration.confirmed ? 'success' : 'danger'}>
                    {registration.confirmed ? 'Confirmed' : 'Pending'}
                  </Badge>
                </TableCell>
                <TableCell>
                  <span className="text-sm">
                    {new Date(registration.recieved).toLocaleDateString()}
                  </span>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <TableFooter>
          <Pagination
            totalResults={totalResults}
            resultsPerPage={resultsPerPage}
            onChange={onPageChangeTable}
            label="Table navigation"
          />
        </TableFooter>
      </TableContainer>
    </AppLayout>
  )
}
