import React, { FunctionComponent, useEffect, useState } from 'react'
import { Broker } from '@bigrig/interfaces'
import { IconContext } from 'react-icons'
import { useNavigate, RouteComponentProps } from '@reach/router'
import { RiAddCircleFill } from 'react-icons/ri'
import {
  Table,
  TableHeader,
  TableCell,
  TableRow,
  TableBody,
  TableFooter,
  TableContainer,
  Avatar,
  Pagination,
} from '@windmill/react-ui'

import { AppLayout } from '../layouts'
import { useAppSelector } from '../redux/hooks'

export const BrokersRoute: FunctionComponent<RouteComponentProps> = () => {
  const { brokers } = useAppSelector(({ brokers: { brokers } }) => ({ brokers }))
  const navigate = useNavigate()

  const [pageTable, setPageTable] = useState(1)
  // @ts-expect-error idc i know this works
  const [dataTable, setDataTable] = useState<Broker[]>(brokers)

  const resultsPerPage = 10
  const totalResults = brokers.length

  const onPageChangeTable = (p: number) => setPageTable(p)

  useEffect(
    // @ts-expect-error idc i know this works
    () => setDataTable(brokers.slice((pageTable - 1) * resultsPerPage, pageTable * resultsPerPage)),
    [pageTable, brokers],
  )

  return (
    <AppLayout>
      <IconContext.Provider value={{ color: '#FF2247', size: '1.5em' }}>
        <RiAddCircleFill className="cursor-pointer m-6" onClick={() => navigate('/brokers/new')} />
      </IconContext.Provider>

      <TableContainer className="mb-8">
        <Table>
          <TableHeader>
            <tr>
              <TableCell>ID</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Phone</TableCell>
            </tr>
          </TableHeader>
          <TableBody>
            {dataTable.map(row => (
              <TableRow key={row.id}>
                <TableCell>
                  <p>{row.id}</p>
                </TableCell>
                <TableCell>
                  <div className="flex items-center text-sm">
                    <Avatar
                      className="hidden mr-3 md:block"
                      src={`${row.avatarURL}`}
                      alt="User avatar"
                    />
                    <div>
                      <p className="font-semibold">{row.name}</p>
                      <p className="text-xs text-gray-600 dark:text-gray-400">{row.email}</p>
                    </div>
                  </div>
                </TableCell>
                <TableCell>
                  <p>{row.email}</p>
                </TableCell>
                <TableCell>
                  <a href={`tel:${row.phone}`}>{row.phone}</a>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <TableFooter>
          <Pagination
            totalResults={totalResults}
            resultsPerPage={resultsPerPage}
            onChange={onPageChangeTable}
            label="Table navigation"
          />
        </TableFooter>
      </TableContainer>
    </AppLayout>
  )
}
