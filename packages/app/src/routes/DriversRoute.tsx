import React, { FunctionComponent, useEffect, useState } from 'react'
import { RouteComponentProps } from '@reach/router'
import { Employee } from '@bigrig/interfaces'
import { JSONSchema7 } from 'json-schema'
import { AiFillEdit } from 'react-icons/ai'
import { RiAddCircleFill } from 'react-icons/ri'
import {
  Table,
  TableHeader,
  TableCell,
  TableRow,
  TableBody,
  TableFooter,
  TableContainer,
  Pagination,
} from '@windmill/react-ui'

import { AppLayout } from '../layouts'
import { FormModal, UnassignModal, AssignModal, VehicleBadge } from '../components'
import { useAppDispatch } from '../redux/store'
import { useAppSelector } from '../redux/hooks'
import { unassignEquipment } from '../redux/features/drivers/driverThunk'

export const DriversRoute: FunctionComponent<RouteComponentProps> = () => {
  const dispatch = useAppDispatch()
  const { drivers, carriers, ui } = useAppSelector(
    ({ ui, drivers: { drivers }, carriers: { carriers } }) => ({
      drivers,
      carriers,
      ui,
    }),
  )

  const [pageTable, setPageTable] = useState(1)
  const [dataTable, setDataTable] = useState<Employee[]>(drivers)

  const resultsPerPage = 10
  const totalResults = drivers.length
  const addDriverSchema: JSONSchema7 = {
    title: 'New Driver Form',
    type: 'object',
    definitions: {
      largeEnum: {
        type: 'string',
        enum: carriers.map(carrier => carrier.name),
      },
    },
    properties: {
      carrier: {
        title: 'Carrier',
        $ref: '#/definitions/largeEnum',
      },
      firstName: { title: 'First name', type: 'string' },
      lastName: { title: 'Last name', type: 'string' },
      email: { title: 'Email', type: 'string' },
      phone: { title: 'Phone', type: 'integer' },
      fax: { title: 'Fax', type: 'integer' },
      DOT: { title: 'DOT', type: 'integer' },
      MC: { title: 'MC', type: 'integer' },
      EIN: { title: 'EIN', type: 'integer' },
      SCAC: { title: 'SCAC', type: 'integer' },
    },
  }

  const onPageChangeTable = (p: number) => setPageTable(p)

  useEffect(() => {
    const filteredData = drivers.filter(
      row => row.email.includes(ui.searchTarget) || row.phone.toString().includes(ui.searchTarget),
    )
    setDataTable(filteredData.slice((pageTable - 1) * resultsPerPage, pageTable * resultsPerPage))
  }, [ui.searchTarget, drivers, pageTable])

  return (
    <AppLayout>
      <FormModal schema={addDriverSchema} />
      <TableContainer className="mb-8">
        <Table>
          <TableHeader>
            <tr>
              <TableCell>ID</TableCell>
              <TableCell>firstName</TableCell>
              <TableCell>lastName</TableCell>
              <TableCell>Vehicle Type</TableCell>
              <TableCell>VIN</TableCell>
              <TableCell>Edit</TableCell>
              <TableCell>email</TableCell>
              <TableCell>phone</TableCell>
              <TableCell>fax</TableCell>
              <TableCell>street</TableCell>
              <TableCell>state</TableCell>
              <TableCell>zip</TableCell>
              <TableCell>DOT</TableCell>
              <TableCell>MC</TableCell>
              <TableCell>EIN</TableCell>
              <TableCell>SCAC</TableCell>
            </tr>
          </TableHeader>
          <TableBody>
            {dataTable.map(row => (
              <TableRow key={row.id}>
                <TableCell>
                  <p>{row.id}</p>
                </TableCell>
                <TableCell>
                  <p>{row.firstName}</p>
                </TableCell>
                <TableCell>
                  <p>{row.lastName}</p>
                </TableCell>
                <TableCell>
                  <VehicleBadge type={row.equipment ? row.equipment.type : undefined} />
                </TableCell>
                <TableCell>
                  <p>{row.equipment ? row.equipment!.vin : ''}</p>
                </TableCell>
                <TableCell>
                  {row.equipment ? (
                    <div className="flex">
                      <AssignModal Icon={AiFillEdit} driver={row} />
                      <UnassignModal
                        action={() => dispatch(unassignEquipment({ employeeID: row.id }))}
                        driverName={row.firstName + ' ' + row.lastName}
                        equipmentType={row.equipment!.type}
                      />
                    </div>
                  ) : (
                    <AssignModal Icon={RiAddCircleFill} driver={row} />
                  )}
                </TableCell>
                <TableCell>
                  <a href={`mailto:${row.email}`} target="_blank" rel="noreferrer">
                    {row.email}
                  </a>
                </TableCell>
                <TableCell>
                  <a href={`tel:${row.phone}`} target="_blank" rel="noreferrer">
                    {row.phone}
                  </a>
                </TableCell>
                <TableCell>
                  <p>{row.fax}</p>
                </TableCell>
                <TableCell>
                  <p>{row.street}</p>
                </TableCell>
                <TableCell>
                  <p>{row.state}</p>
                </TableCell>
                <TableCell>
                  <a
                    href={`https://www.google.com/maps/place/${row.zip}`}
                    target="_blank"
                    rel="noreferrer"
                  >
                    {row.zip}
                  </a>
                </TableCell>
                <TableCell>
                  <p>{row.DOT}</p>
                </TableCell>
                <TableCell>
                  <p>{row.MC}</p>
                </TableCell>
                <TableCell>
                  <p>{row.EIN}</p>
                </TableCell>
                <TableCell>
                  <p>{row.SCAC}</p>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        <TableFooter>
          <Pagination
            totalResults={totalResults}
            resultsPerPage={resultsPerPage}
            onChange={onPageChangeTable}
            label="Table navigation"
          />
        </TableFooter>
      </TableContainer>
    </AppLayout>
  )
}
