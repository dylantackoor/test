import React, { FunctionComponent } from 'react'
import { RouteComponentProps } from '@reach/router'
import { MdBlock } from 'react-icons/md'

import { AppLayout } from '../layouts'

export const NotFoundRoute: FunctionComponent<RouteComponentProps> = () => (
  <AppLayout>
    <MdBlock className="w-12 h-12 mt-8 text-red-200" aria-hidden="true" />
    <h1 className="text-6xl font-semibold text-gray-700 dark:text-gray-200">404</h1>
    <p className="text-gray-700 dark:text-gray-300">
      Page not found. Check the address or{' '}
      <a className="text-red-600 hover:underline dark:text-red-300" href="/">
        go home
      </a>
      .
    </p>
  </AppLayout>
)
