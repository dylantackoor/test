import React, { useState, FunctionComponent } from 'react'
import { useNavigate } from '@reach/router'
import { RouteComponentProps } from '@reach/router'
import { Button, Label, Input } from '@windmill/react-ui'
import { BrokerCreationRequest } from '@bigrig/interfaces'

import { AppLayout } from '../layouts'
import { Annotator, MyForm } from '../components'
import { T_Highlight } from '../types/react-pdf-highlighter'
import { useAppDispatch } from '../redux/store'
import { addBroker, saveFile } from '../redux/features/brokers/brokerThunk'

export const BrokersFormRoute: FunctionComponent<RouteComponentProps> = () => {
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const [currentStep, setStep] = useState(1)
  const [pdfFile, setPdfFile] = useState<File | null>(null)

  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [avatarURL, setAvatarURL] = useState('')
  const [mappedInputs, setMappedInputs] = useState<Array<T_Highlight>>([])

  const addMappedInput = (newInput: T_Highlight) => setMappedInputs([...mappedInputs, newInput])

  const handleSubmit = () => {
    if (!pdfFile) {
      return
    }

    const newBrokerRequestBody: BrokerCreationRequest = {
      name,
      email,
      phone,
      avatarURL,
      packets: [
        {
          version: 1,
          bytes: '',
          inputs: mappedInputs.map(
            ({
              comment: { text },
              content: { image },
              position: {
                pageNumber,
                boundingRect: { x1, x2, y1, y2 },
              },
            }) => ({
              column: text,
              preview: image!,
              position: {
                page: pageNumber,
                x1,
                x2,
                y1,
                y2,
              },
            }),
          ),
        },
      ],
    }

    dispatch(saveFile({ pdfFile }))
    dispatch(addBroker(newBrokerRequestBody))
    navigate('/brokers')
  }

  return (
    <AppLayout>
      <h1>Add Broker Forms</h1>
      <div>
        <Button style={{ backgroundColor: '#FF2247' }} onClick={() => setStep(1)}>
          Step 1
        </Button>
        <Button style={{ backgroundColor: '#FF2247' }} onClick={() => setStep(2)}>
          Step 2
        </Button>
      </div>

      {currentStep === 1 ? (
        <>
          <form>
            <h1>Add Broker Details</h1>
            <Label>
              <span>Name:</span>
              <Input
                css={{}}
                value={name}
                onChange={e => setName(e.target.value)}
                style={{ outline: 'black solid 1px' }}
                className="mt-1"
              />
            </Label>
            <Label>
              <span>Email:</span>

              <Input
                css={{}}
                value={email}
                onChange={e => setEmail(e.target.value)}
                style={{ outline: 'black solid 1px' }}
                className="mt-1"
              />
            </Label>

            <Label>
              <span>Phone:</span>

              <Input
                css={{}}
                value={phone}
                onChange={e => setPhone(e.target.value)}
                style={{ outline: 'black solid 1px' }}
                className="mt-1"
              />
            </Label>

            <Label>
              <span>Avatar URL:</span>

              <Input
                css={{}}
                value={avatarURL}
                onChange={e => setAvatarURL(e.target.value)}
                style={{ outline: 'black solid 1px' }}
                className="mt-1"
              />
            </Label>
            <Button style={{ backgroundColor: '#FF2247' }} onClick={() => setStep(2)}>
              Next
            </Button>
          </form>
        </>
      ) : (
        <div className="flex" style={{ height: '85vh' }}>
          <div
            className="w-1/2"
            style={{
              height: '100%',
              width: '50vw',
              position: 'relative',
            }}
          >
            <Annotator addMappedInput={addMappedInput} pdfFile={pdfFile} setPdfFile={setPdfFile} />
          </div>
          <div className="w-1/2" style={{ overflowY: 'scroll' }}>
            <MyForm mappedInputs={mappedInputs} onSubmit={handleSubmit} />
          </div>
        </div>
      )}
    </AppLayout>
  )
}
