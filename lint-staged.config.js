module.exports = {
  '*.{js,jsx,ts,tsx,d.ts,json,md,yml,yaml}': ['prettier --write'],
  '*.{js,jsx,ts,tsx,d.ts}': 'eslint --fix --cache --cache-location .eslintcache',
  '*.{css,jsx,tsx}': 'stylelint',
  '*.md': 'markdownlint --fix',
}
